'use strict'
var path = require('path');
var Jimp = require('jimp');
var Promise = require('es6-promise').Promise;

var textLib = require('./texts');
var utils = require('./fontutil');

var LRU = require("lru-cache"),
    cache = LRU({
        maxAge: 3600000,
    });

var getJimpObj = function(entity) {
		var cacheKey;
		if (typeof(entity) == 'string')
        	cacheKey = 'entity/{0}'.format(entity);
        if(cacheKey) {
            var cachePromise = cache.get(cacheKey);
            if (cachePromise) {
                return cachePromise;
            };
        }

		var promise;
		promise = new Promise(function(resolve, reject) {
			Jimp.read(entity, function (err, img) {
				if(err)
					reject(err)
				else
					resolve(img);
			});
		});

        if(cacheKey) {
	        cache.set(cacheKey, promise);
	    };
		return promise;
	}

var getJimpObjFromFile = function(fileName) {
		return getJimpObj(fileName);
	}

var getJimpObjFromFileBuffer = function(buffer) {
		return getJimpObj(buffer);
	}

var getImagesFileNames = function (formImages, params) {
        var fileList = {};
        var bgschema = params.bgschema;

        if ('photo_dark' == bgschema) {
            fileList['logo'] = path.join(__dirname, './source/images/logo_taxi_w.svg');
            fileList['bg'] = path.join(__dirname, './source/images/bg_photo_dark.png');
            fileList['close_button'] = path.join(__dirname, './source/images/cross_w.svg');
            fileList['install_button'] = path.join(__dirname, './source/images/btn_install_y.png');
        } else if('photo_light' == bgschema) {
            fileList['logo'] = path.join(__dirname, './source/images/logo_taxi_b.svg');
            fileList['bg'] = path.join(__dirname, './source/images/bg_photo_light.png');
            fileList['close_button'] = path.join(__dirname, './source/images/cross_b.svg');
            fileList['install_button'] = path.join(__dirname, './source/images/btn_install_y.png');
        } else if ('yellow' == bgschema) {
            fileList['logo'] = path.join(__dirname, './source/images/logo_taxi_b.svg');
            fileList['ticket'] = path.join(__dirname, './source/images/ticket_w.png');
            fileList['close_button'] = path.join(__dirname, './source/images/cross_b.svg');
            fileList['install_button'] = path.join(__dirname, './source/images/btn_install_w.png');
        } else if ('white' == bgschema) {
            fileList['logo'] = path.join(__dirname, './source/images/logo_taxi_yb.svg');
            fileList['ticket'] = path.join(__dirname, './source/images/ticket_y.png');
            fileList['close_button'] = path.join(__dirname, './source/images/cross_b.svg');
            fileList['install_button'] = path.join(__dirname, './source/images/btn_install_y.svg');
        } else { // black
            fileList['logo'] = path.join(__dirname, './source/images/logo_taxi_yw.svg');
            fileList['ticket'] = path.join(__dirname, './source/images/ticket_y.png');
            fileList['close_button'] = path.join(__dirname, './source/images/cross_w.svg');
            fileList['install_button'] = path.join(__dirname, './source/images/btn_install_y.png');
        }
        fileList['mobilelogo'] = path.join(__dirname, './source/images/ios_logo.png');
        fileList['gp_button'] = path.join(__dirname, './source/images/gp_button.png');
        fileList['as_button'] = path.join(__dirname, './source/images/as_button.png');

        var btntype = params.btntype;
        fileList.install_button = path.join(__dirname, './source/images/btn_install_y.svg')
        // if ('ydownload' == btntype)
        //     fileList.install_button = path.join(__dirname, './source/images/btn_download_y.png')
        // else if ('ydownloadapp' == btntype)
        //     fileList.install_button = path.join(__dirname, './source/images/btn_downloadapp_y.png')
        // else if ('yinstall' == btntype)
        //     fileList.install_button = path.join(__dirname, './source/images/btn_install_y.svg')
        // else if ('was' == btntype)
        //     fileList.install_button = path.join(__dirname, './source/images/as_button_b.png')
        // else if ('wgp' == btntype)
        //     fileList.install_button = path.join(__dirname, './source/images/gp_button_b.png')
        // else if ('bas' == btntype)
        //     fileList.install_button = path.join(__dirname, './source/images/as_button_b.png')
        // else if ('bgp' == btntype)
        //     fileList.install_button = path.join(__dirname, './source/images/gp_button_b.png')
        // else if ('yknowmore' == btntype)
        //     fileList.install_button = path.join(__dirname, './source/images/btn_more.png')
        // else
        //     fileList.install_button = path.join(__dirname, './source/images/btn_download_y.png');

        return fileList;
    }
exports.getImagesFileNames = getImagesFileNames;

var drawBackground = function (bgImg, params) {
        if(!bgImg)
            return Promise.resolve(false);

        var imgHeight = params.imgHeight,
            imgWidth = params.imgWidth;

        var promise;
        promise = new Promise(function (resolve, reject) {
            var im = new Jimp(imgWidth, imgHeight, function(err, img) {
                if(err) {
                    reject(err);
                    return;
                };

                bgImg.crop(5, 10, bgImg.bitmap.width-10, bgImg.bitmap.height-20) //margin(5px, 10px)

                var q;
                if(bgImg.bitmap.height/imgHeight > bgImg.bitmap.width/imgWidth) {
                    q = imgWidth / bgImg.bitmap.width;
                } else {
                    q = imgHeight / bgImg.bitmap.height;
                };

                var width = bgImg.bitmap.width*q;
                var height = bgImg.bitmap.height*q;
                var left, top;


                left = parseInt((width-imgWidth)/2);
                top = parseInt((height-imgHeight)/2);

                bgImg.resize(parseInt(width), parseInt(height)).crop(left, top, imgWidth, imgHeight)
                resolve(bgImg);
            });
        });

        return promise;
    }
exports.drawBackground = drawBackground;

var drawTicket = function (ticketImg, params) {
        if(!ticketImg)
            return Promise.resolve(false);

        var imgHeight = params.imgHeight,
            imgWidth = params.imgWidth,
            ticketHeight = params.ticketHeight;

        var propHeight = ticketHeight,
            propWidth = parseInt((ticketImg.bitmap.width * propHeight) / ticketImg.bitmap.height);
        var tickets = params.ticketCenters.map(function(elem) {
                    return {
                        left: elem.left - parseInt(propWidth / 2),
                        top: elem.top - parseInt(propHeight / 2)
                    }
                });


        var promise;
        promise = new Promise(function (resolve, reject) {
            var im = new Jimp(imgWidth+2, imgHeight+2, function(err, img) {
                if(err) {
                    reject(err);
                    return;
                }

                for(var i=0; i<tickets.length; i++) {
                    img.composite(ticketImg.resize(Jimp.AUTO, ticketHeight), tickets[i].left+1, tickets[i].top+1);
                }
                img.crop(1, 1, imgWidth, imgHeight);
                resolve(img);
            });
        });

        return promise;
    }
exports.drawTicket = drawTicket;

var drawInstallBtn = function (btnImg, params) {
        var imgHeight = params.imgHeight,
            imgWidth = params.imgWidth,
            btnLeft = params.btnLeft,
            btnTop = params.btnTop,
            btnHeight = params.btnHeight;

        var promise;
        promise = new Promise(function (resolve, reject) {
            var im = new Jimp(imgWidth, imgHeight, function(err, img) {
                if(err) {
                    reject(err);
                    return;
                }

                btnImg.resize(Jimp.AUTO, btnHeight);
                img.composite(btnImg, btnLeft, btnTop);

                resolve(img);
            });
        });

        return promise;
    }
exports.drawInstallBtn = drawInstallBtn;

var drawLogo = function (logoImg, params) {
        var imgHeight = params.imgHeight,
            imgWidth = params.imgWidth,
            logoLeft = params.logoLeft,
            logoTop = params.logoTop,
            logoHeight = params.logoHeight;

        var promise;
        promise = new Promise(function (resolve, reject) {
            var im = new Jimp(imgWidth, imgHeight, function(err, img) {
                if(err) {
                    reject(err);
                    return;
                }

                logoImg.resize(Jimp.AUTO, logoHeight);
                img.composite(logoImg, logoLeft, logoTop);

                resolve(img);
            });
        });

        return promise;
    }
exports.drawLogo = drawLogo;

var drawBorder = function (img, params) { // img - gm image
        var x0 = 0, y0 = 0,
            x1 = params.width-1, y1 = y0,
            x2 = x1, y2 = params.height-1,
            x3 = x0, y3 = y2;

        img.drawLine(x0, y0, x1, y1);
        img.drawLine(x1, y1, x2, y2);
        img.drawLine(x2, y2, x3, y3);
        img.drawLine(x3, y3, x0, y0);
    }
exports.drawBorder = drawBorder;

var drawLabel = function(img, bgschema, label, textConfig, bannerWidth) {
        //возвращает false - если надпись занимает не меньше заданного в textConfig.lines параметра
        //иначе фактическое количество строк
        var labelParts = textLib.getSplittedLabelList(label, {
                bgschema: bgschema,
                fieldWidth: textConfig.fieldWidth,
                fontSize: textConfig.fontSize,
                shift: textConfig.shift,
                dashshift: textConfig.dashshift,
            });

        var top = textConfig.top,
            left = textConfig.left,
            lineWidth;

        if(textConfig.align == 'center') {
            lineWidth = utils.getTextWidth(labelParts[0],textConfig.fontSize);
            left = parseInt((bannerWidth - lineWidth) / 2);
        };
        img
        .font(
            utils.fontFileName,
            textConfig.fontSize
        )
        .drawText(
            left,
            top,
            labelParts[0]
        );

        if(labelParts[1] && textConfig.lines>1) {
            if(textConfig.align == 'center') {
                lineWidth = utils.getTextWidth(labelParts[1],textConfig.fontSize);
                left = parseInt((bannerWidth - lineWidth) / 2);
            };
            img
            .font(
                utils.fontFileName,
                textConfig.fontSize
            )
            .drawText(
                left,
                top + textConfig.lineInterval,
                labelParts[1]
            );
        };


        if(labelParts[2] && textConfig.lines>2) {
            if(textConfig.align == 'center') {
                lineWidth = utils.getTextWidth(labelParts[2],textConfig.fontSize);
                left = parseInt((bannerWidth - lineWidth) / 2);
            };
            img
            .font(
                utils.fontFileName,
                textConfig.fontSize
            )
            .drawText(
                left,
                top + 2 * textConfig.lineInterval,
                labelParts[2]
            );
        };

        return (labelParts.length < textConfig.lines) ? labelParts.length : false;
    };
exports.drawLabel = drawLabel;