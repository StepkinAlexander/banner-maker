'use strict'
var Promise = require('es6-promise').Promise;
var Jimp = require('jimp');
var gm = require('gm').subClass({imageMagick: true});
var getImageType = require('image-type');

var pnfs = require('pn/fs');
var svg2png = require('svg2png');

var jimpLib = require("./jimp");
var colorLib = require('./colors');
var logger = require('./logger');

const fs = require('fs');
const path = require('path');
const bannersDir = path.join(__dirname, '../../', require('cfg').bannersTmp);

var getBannerBuffer = function (params, formImages, bannerConfig) {
    var background = colorLib.getBackgroundColor(params.bgschema, bannerConfig.width, bannerConfig.height);
    var fontColor = colorLib.getFontColor(params.bgschema);
    var ageFontColor = colorLib.getAgeFontColor(params.bgschema);
    var footerFontColor = colorLib.getFooterFontColor(params.bgschema);
    var fileList = jimpLib.getImagesFileNames(formImages, params);

    var promise;
    promise = new Promise(function (resolve, reject) {
        var banner;
        banner = new Jimp(bannerConfig.width, bannerConfig.height, background, function (err, image) {
            var promisesList = [];

            var bgImg;
            var needBg = ['photo_dark', 'photo_light'].indexOf(params.bgschema) != -1;
            if(needBg && formImages && formImages.custombg) {
                if (formImages.custombg) {
                    var imageType = getImageType(formImages.custombg);
                    if(imageType && (['png', 'jpg'].indexOf(imageType.ext) != -1))
                        bgImg = Jimp.read(formImages.custombg)
                    else
                        bgImg = svg2png(
                                    formImages.custombg,
                                    { width: bannerConfig.width, height: bannerConfig.height }
                                )
                                .then(function(buffer) {
                                    return Jimp.read(buffer)
                                });
                }
            } else if(needBg) {
                bgImg = (fileList.bg)
                        ?   (fileList.bg.match(/\.svg$/))
                            ?   pnfs
                                .readFile(fileList.bg)
                                .then(function(buffer) {
                                    return svg2png(
                                                buffer,
                                                { width: bannerConfig.width, height: bannerConfig.height }
                                            )
                                })
                                .then(function(buffer) {
                                    return Jimp.read(buffer);
                                })
                            :   Jimp.read(fileList.bg)
                        : Promise.resolve(false);
            } else {
                bgImg = Promise.resolve(false);
            };
            promisesList.push(bgImg);

            //if ticket
            var tcktImg;
            if(bannerConfig.ticket) {
                if (formImages.customimg) {
                    var imageType = getImageType(formImages.customimg);
                    if(imageType && (['png', 'jpg'].indexOf(imageType.ext) != -1))
                        tcktImg = Jimp
                                .read(formImages.customimg)
                    else
                        tcktImg = svg2png(
                            formImages.customimg,
                            { height: bannerConfig.ticket.height }
                        )
                        .then(function(buffer) {
                            return Jimp.read(buffer);
                        })
                } else {
                    tcktImg = Promise.resolve(false);
                };
            } else {
                tcktImg = Promise.resolve(false);
            };
            promisesList.push(tcktImg);

            //if logo
            if (bannerConfig.logo)
                promisesList.push(
                    (bannerConfig.logo.mobile)
                    ?   (fileList.mobilelogo.match(/\.svg$/))
                        ?   pnfs
                            .readFile(fileList.mobilelogo)
                            .then(function(buffer) {
                                return svg2png(buffer, { height: bannerConfig.logo.height });
                            })
                            .then(function(buffer) {
                                return Jimp.read(buffer);
                            })
                        :   Jimp.read(fileList.mobilelogo)
                    :   (fileList.logo.match(/\.svg$/))
                        ?   pnfs
                            .readFile(fileList.logo)
                            .then(function(buffer) {
                                return svg2png(buffer, { height: bannerConfig.logo.height });
                            })
                            .then(function(buffer) {
                                return Jimp.read(buffer);
                            })
                        :   Jimp.read(fileList.logo)
                )
            else
                promisesList.push(Promise.resolve(false));

            //if button
            if (bannerConfig.button)
                promisesList.push(
                    (fileList.install_button.match(/\.svg$/))
                    ?   pnfs
                        .readFile(fileList.install_button)
                        .then(function(buffer) {
                            return svg2png(buffer, { height: bannerConfig.button.height });
                        })
                        .then(function(buffer) {
                            return Jimp.read(buffer);
                        })
                    :   Jimp.read(fileList.install_button)
                )
            else
                promisesList.push(Promise.resolve(false));

            //if close
            if (bannerConfig.close)
                promisesList.push(
                    (fileList.close_button.match(/\.svg$/))
                    ?   pnfs
                        .readFile(fileList.close_button)
                        .then(function(buffer) {
                            return svg2png(buffer, { height: bannerConfig.close.height });
                        })
                        .then(function(buffer) {
                            return Jimp.read(buffer);
                        })
                    :   Jimp.read(fileList.close_button)
                )
            else
                promisesList.push(Promise.resolve(false));
            Promise
            .all(promisesList)
            .then(function (images) {
                var pmses = [
                        jimpLib.drawBackground(images[0], {
                            imgHeight: bannerConfig.height,
                            imgWidth: bannerConfig.width,
                            imgCenterTop: (bannerConfig.bg && bannerConfig.bg.center && bannerConfig.bg.center.top) ? bannerConfig.bg.center.top : false,
                            imgCenterLeft: (bannerConfig.bg && bannerConfig.bg.center && bannerConfig.bg.center.left) ? bannerConfig.bg.center.left : false,
                        }),
                    ]

                pmses.push(
                    (bannerConfig.ticket)
                    ?   jimpLib.drawTicket(images[1], {
                            imgHeight: bannerConfig.height,
                            imgWidth: bannerConfig.width,
                            ticketCenters: (function(config) {
                                    return Array.isArray(config) ? config : [config];
                                })(bannerConfig.ticket.center),
                            ticketHeight: bannerConfig.ticket.height,
                        })
                    :   Promise.resolve(false)
                );

                pmses.push(
                    (bannerConfig.logo)
                    ?   jimpLib.drawLogo(images[2], {
                            imgHeight: bannerConfig.height,
                            imgWidth: bannerConfig.width,
                            logoLeft: bannerConfig.logo.left,
                            logoTop: bannerConfig.logo.top,
                            logoHeight: bannerConfig.logo.height,
                            logoAlign: bannerConfig.logo.align,
                        })
                    :   Promise.resolve(false)
                );

            
                pmses.push(
                    (bannerConfig.button)
                    ?   jimpLib.drawInstallBtn(images[3], {
                            imgHeight: bannerConfig.height,
                            imgWidth: bannerConfig.width,
                            btnLeft: bannerConfig.button.left,
                            btnTop: bannerConfig.button.top,
                            btnHeight: bannerConfig.button.height,
                            btnAlign: bannerConfig.button.align,
                        })
                    :   Promise.resolve(false)
                );

                pmses.push(
                    (bannerConfig.close)
                    ?   jimpLib.drawInstallBtn(images[4], {
                            imgHeight: bannerConfig.height,
                            imgWidth: bannerConfig.width,
                            btnLeft: bannerConfig.close.left,
                            btnTop: bannerConfig.close.top,
                            btnHeight: bannerConfig.close.height,
                        })
                    :   Promise.resolve(false));

                return Promise.all(pmses);
            })
            .then(function(values) {
                for(var i=0;i<values.length;i++)
                    if(values[i])
                        image.composite(values[i], 0, 0);

                image.getBuffer(Jimp.MIME_PNG, function(err, buffer) {
                    if(err) {
                        reject(err); return
                    };
                    var img = gm(buffer);

                    //if border
                    img.fill('rgba(0, 0, 0, 0.4)');
                    if(bannerConfig.border)
                        jimpLib.drawBorder(img, bannerConfig);

                    img.fill(ageFontColor);
                    //Написать Возрастное ограничение
                    if(bannerConfig.text.age)
                        jimpLib.drawLabel(
                            img,
                            params.bgschema,
                            params.ageplus,
                            bannerConfig.text.age,
                            bannerConfig.width
                        );

                    //Написать кастомный текст лого
                    img.fill(fontColor);
                    if(bannerConfig.text.logo)
                        jimpLib.drawLabel(
                            img,
                            params.bgschema,
                            params.logotitle,
                            bannerConfig.text.logo,
                            bannerConfig.width
                        );

                    //Написать Заголовок
                    img.fill(fontColor);
                    var shortTitle;
                    if(bannerConfig.text.title)
                        shortTitle = jimpLib.drawLabel(
                            img,
                            params.bgschema,
                            params.title,
                            bannerConfig.text.title,
                            bannerConfig.width
                        );

                    //Написать Подзаголовок
                    var cloneObj = function(obj) {
                            var clone = {};
                            for(var key in obj)
                                clone[key] = obj[key];
                            return clone;
                        }
                    img.fill(fontColor);
                    if(bannerConfig.text.subTitle) {
                        var subConfig = cloneObj(bannerConfig.text.subTitle);

                        if(!!shortTitle)
                            subConfig.top = bannerConfig.text.title.top + shortTitle * bannerConfig.text.title.lineInterval;

                        jimpLib.drawLabel(
                            img,
                            params.bgschema,
                            params.subtitle,
                            subConfig,
                            bannerConfig.width
                        );
                    };

                    //Написать Подвал
                    img.fill(footerFontColor);
                    if(bannerConfig.text.footer)
                        jimpLib.drawLabel(
                            img,
                            params.bgschema,
                            params.footer,
                            bannerConfig.text.footer,
                            bannerConfig.width
                        );

                    img
                    .setFormat('png')
                    .toBuffer(function (err, buffer) {
                        resolve(buffer);

                        var userDir = path.join(bannersDir, params.csrf.replace(/\W/g,''));
                        if (!fs.existsSync(userDir)){
                            fs.mkdirSync(userDir);
                        };

                        var filename = path.join(userDir, '{0}_{1}.png'.format(params.scale, params.slide))
                        Jimp.read(buffer, function(err, img) {
                            if(err) logger.info('ERROR! ', filename, err.stack)
                            img.write(filename, function (err, data) {
                                if(err) logger.info('err: ', filename, err)
                            });
                        })
                    });
                })
            })
            .catch(function(err) {
                reject(err);
            });
        })
    });

    return promise;
}

var getBanner = function (params, formImages, bannerConfig) {
    var promise;
    promise = new Promise(function (resolve, reject) {
        getBannerBuffer(params, formImages, bannerConfig)
        .then(function (buffer) {
            var data = {
                    format: '{0}x{1}'.format(bannerConfig.width, bannerConfig.height),
                    comment: bannerConfig.comment,
                    border: (params.bgschema=='white') ? '#e5e5e5' : '',
                    image: 'data:image/png;base64,' + buffer.toString('base64')
                };
            resolve(data);
        })
        .catch(function(err) {
            reject(err);
        });
    });

    return promise;
}
exports.getBanner = getBanner;
