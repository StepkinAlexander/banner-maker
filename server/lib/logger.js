var config = require('cfg');
var path = require('path');
var log4js = require('log4js'); 

log4js.configure({
  appenders: [
    {
        type: 'console'
    },
    {
        type: 'file',
        filename: path.join(config.logger.dir, config.logger.filename),
        maxLogSize: 20480,
        backups: 0,
        category: 'cheese'
    }
  ]
});

var logger = log4js.getLogger('cheese');
logger.setLevel('INFO');

module.exports = logger;