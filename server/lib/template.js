'use strict'
var Promise = require('es6-promise').Promise;

var priv = require('../../static/desktop.pages/index/_index.all.priv.js');

var LRU = require("lru-cache"),
    cache = LRU({
        maxAge: 3600000,
    });

exports.getTld = function (req) {
	return req.headers.host && req.headers.host.split('yandex.')[1] || 'ru';
};


exports.render = function (req, blockName, data) {
	var iGlobal, bPage, html, promise;

	var cacheKey = 'html_' + req.csrfToken;
    var cachePromise = cache.get(cacheKey);

    if (cachePromise) {
        return cachePromise;
    };

    promise = new Promise(function(resolve, reject) {
    	try {
		    iGlobal = priv.blocks['i-global']('ru', req.tld);
		    bPage = priv.blocks[blockName](req, data);
		    html = priv.BEMHTML.apply([iGlobal, bPage]);

		    resolve(html);
		} catch (err) {
			reject(err);
		}
    })
    cache.set(cacheKey, promise);

	return promise;
}

exports.renderBlock = function (req, blockName, data) {
	var bPage, html, promise;

    promise = new Promise(function(resolve, reject) {
    	try {
		    bPage = priv.blocks[blockName](req, data);
		    html = priv.BEMHTML.apply([bPage]);

		    resolve(html);
		} catch (err) {
			reject(err);
		}
    })

	return promise;
}