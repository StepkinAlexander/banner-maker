module.exports = [{
        width: 320,
        height: 50,
        comment: 'Admob [слой 1]',
        logo: {
            mobile: true,
            top: 8,
            left: 8,
            height: 34,
        },
        ticket: {
            center: {
                top: 25,
                left: 296
            },
            height: 44,
        },
        text: {
            age: {
                fontSize: 10,
                fieldWidth: 100500,
                lineInterval: 0,
                lines: 1,
                top: 11,
                left: 306,
            },
            title: {
                fontSize: 15,
                fieldWidth: 255,
                lineInterval: 10,
                lines: 1,
                top: 39,
                left: 53,
            },
            logo: {
                fontSize: 15,
                fieldWidth: 230,
                lineInterval: 0,
                lines: 1,
                top: 23,
                left: 52,
            },
        }
    },
    {
        width: 320,
        height: 50,
        comment: 'Admob [слой 2]',
        logo: {
            mobile: true,
            top: 8,
            left: 8,
            height: 34,
        },
        ticket: {
            center: {
                top: 25,
                left: 297
            },
            height: 44,
        },
        text: {
            age: {
                fontSize: 10,
                fieldWidth: 100500,
                lineInterval: 0,
                lines: 1,
                top: 11,
                left: 306,
            },
            footer: {
                fontSize: 10,
                fieldWidth: 240,
                lineInterval: 12,
                lines: 3,
                top: 17,
                left: 52,
            }
        }
    }
];