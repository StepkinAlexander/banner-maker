module.exports = [{
    width: 1242,
    height: 360,
    comment: 'с фоном шириной 1242x360 (края фона будут обрезаться) Sports.ru',
    logo: {
        mobile: false,
        top: 41,
        left: 540,
        height: 45,
    },
    button: {
        top: 262,
        left: 530,
        height: 53,
    },
    ticket: {
        center: [
            {
                top: 182,
                left: 1060
            },
            {
                top: 182,
                left: 191
            },
        ],
        height: 335,
    },
    text: {
        age: {
            fontSize: 22,
            fieldWidth: 100500,
            lineInterval: 0,
            lines: 1,
            top: 25,
            left: 1206,
        },
        title: {
            fontSize: 32,
            fieldWidth: 580,
            lineInterval: 45,
            align: 'center',
            lines: 1,
            top: 143,
            left: 0,
        },
        subTitle: {
            fontSize: 25,
            fieldWidth: 370,
            lineInterval: 25,
            align: 'center',
            lines: 1,
            top: 182,
            left: 0,
        },
        footer: {
            fontSize: 10,
            fieldWidth: 385,
            lineInterval: 12,
            align: 'center',
            lines: 2,
            top: 335,
            left: 0,
            shift: true
        }
    }
}];