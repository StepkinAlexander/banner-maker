module.exports = [
    {
        width: 960,
        height: 640,
        comment: '',
        logo: {
            top: 60,
            left: 60,
            height: 80,
        },
        button: {
            top: 462,
            left: 60,
            height: 100,
        },
        ticket: {
            center: {
                top: 321,
                left: 805
            },
            height: 405,
        },
        text: {
            age: {
                fontSize: 40,
                fieldWidth: 100500,
                lineInterval: 0,
                lines: 1,
                top: 50,
                left: 893,
            },
            title: {
                fontSize: 48,
                fieldWidth: 560,
                lineInterval: 55,
                lines: 2,
                top: 229,
                left: 65,
            },
            subTitle: {
                fontSize: 33,
                fieldWidth: 515,
                lineInterval: 0,
                lines: 1,
                top: 335,
                left: 63,
            },
            footer: {
                fontSize: 15,
                fieldWidth: 560,
                lineInterval: 17,
                lines: 2,
                top: 601,
                left: 62,
            }
        }
    },
    {
        width: 480,
        height: 320,
        comment: '',
        logo: {
            top: 30,
            left: 30,
            height: 40,
        },
        button: {
            top: 231,
            left: 30,
            height: 50,
        },
        ticket: {
            center: {
                top: 160,
                left: 402
            },
            height: 202,
        },
        text: {
            age: {
                fontSize: 20,
                fieldWidth: 100500,
                lineInterval: 0,
                lines: 1,
                top: 24,
                left: 447,
            },
            title: {
                fontSize: 26,
                fieldWidth: 280,
                lineInterval: 27,
                lines: 2,
                top: 114,
                left: 32,
            },
            subTitle: {
                fontSize: 17,
                fieldWidth: 257,
                lineInterval: 0,
                lines: 1,
                top: 167,
                left: 31,
            },
            footer: {
                fontSize: 8,
                fieldWidth: 260,
                lineInterval: 12,
                lines: 2,
                top: 300,
                left: 31,
            }
        }
    },
];