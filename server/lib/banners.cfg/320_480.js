module.exports = [{
    width: 320,
    height: 480,
    comment: 'Admob full screen',
    logo: {
        top: 50,
        left: 98,
        height: 35,
    },
    button: {
        top: 377,
        left: 78,
        height: 45,
    },
    ticket: {
        center: {
            top: 292,
            left: 160
        },
        height: 158,
    },
    text: {
        age: {
            fontSize: 12,
            fieldWidth: 100500,
            lineInterval: 0,
            lines: 1,
            top: 15,
            left: 303,
        },
        title: {
            fontSize: 25,
            fieldWidth: 240,
            lineInterval: 30,
            align: 'center',
            lines: 2,
            top: 138,
            left: 0,
        },
        subTitle: {
            fontSize: 15,
            fieldWidth: 245,
            lineInterval: 0,
            align: 'center',
            lines: 1,
            top: 194,
            left: 0,
        },
        footer: {
            fontSize: 10,
            fieldWidth: 310,
            lineInterval: 12,
            align: 'center',
            lines: 2,
            top: 457,
            left: 0,
        }
    }
}];