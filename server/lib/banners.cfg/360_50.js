module.exports = [
    {
        width: 360,
        height: 50,
        comment: 'Admob [слой 1]',
        logo: {
            mobile: true,
            top: 8,
            left: 7,
            height: 34,
        },
        ticket: {
            center: {
                top: 25,
                left: 326
            },
            height: 44,
        },
        text: {
            age: {
                fontSize: 9,
                fieldWidth: 100500,
                lineInterval: 0,
                lines: 1,
                top: 11,
                left: 346,
            },
            title: {
                fontSize: 15,
                fieldWidth: 255,
                lineInterval: 10,
                lines: 1,
                top: 40,
                left: 51,
            },
            logo: {
                fontSize: 15,
                fieldWidth: 230,
                lineInterval: 0,
                lines: 1,
                top: 23,
                left: 51,
            },
        }
    },
    {
        width: 360,
        height: 50,
        comment: 'Admob [слой 2]',
        logo: {
            mobile: true,
            top: 8,
            left: 8,
            height: 34,
        },
        ticket: {
            center: {
                top: 25,
                left: 326
            },
            height: 44,
        },
        text: {
            age: {
                fontSize: 9,
                fieldWidth: 100500,
                lineInterval: 0,
                lines: 1,
                top: 11,
                left: 346,
            },
            footer: {
                fontSize: 10,
                fieldWidth: 240,
                lineInterval: 11,
                lines: 3,
                top: 18,
                left: 54,
            }
        }
    },
];