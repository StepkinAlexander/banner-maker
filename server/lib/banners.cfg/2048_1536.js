module.exports = [{
    width: 2048,
    height: 1536,
    logo: {
        top: 151,
        left: 121,
        height: 170,
    },
    button: {
        top: 1150,
        left: 121,
        height: 199,
    },
    ticket: {
        center: {
            top: 770,
            left: 1705
        },
        height: 932,
    },
    close: {
        top: 20,
        left: 1979,
        height: 50,
    },
    text: {
        age: {
            fontSize: 45,
            fieldWidth: 100500,
            lineInterval: 0,
            lines: 1,
            top: 68,
            left: 23,
        },
        title: {
            fontSize: 136,
            fieldWidth: 1230,
            lineInterval: 142,
            lines: 2,
            top: 539,
            left: 130,
        },
        subTitle: {
            fontSize: 80,
            fieldWidth: 1200,
            lineInterval: 0,
            lines: 1,
            top: 793,
            left: 125,
        },
        footer: {
            fontSize: 36,
            fieldWidth: 1320,
            lineInterval: 38,
            lines: 2,
            top: 1452,
            left: 122,
        }
    }
}];
