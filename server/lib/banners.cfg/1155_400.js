module.exports = [
    {
        width: 1155,
        height: 400,
        comment: 'Цукерберг Позвонит и Tjournal.com [слой 1]',
        logo: {
            top: 55,
            left: 50,
            height: 70,
        },
        button: {
            top: 261,
            left: 50,
            height: 85,
        },
        text: {
            age: {
                fontSize: 22,
                fieldWidth: 100500,
                lineInterval: 0,
                lines: 1,
                top: 34,
                left: 1112
            },
            title: {
                fontSize: 60,
                fieldWidth: 1000,
                lineInterval: 0,
                lines: 1,
                top: 208,
                left: 57,
            },
        }
    },
    {
        width: 1155,
        height: 400,
        comment: 'Цукерберг Позвонит и Tjournal.com [слой 2]',
        logo: {
            top: 55,
            left: 50,
            height: 70,
        },
        text: {
            age: {
                fontSize: 22,
                fieldWidth: 100500,
                lineInterval: 0,
                lines: 1,
                top: 34,
                left: 1112
            },
            footer: {
                fontSize: 42,
                fieldWidth: 955,
                lineInterval: 68,
                lines: 3,
                top: 206,
                left: 50,
                shift: true,
            }
        }
    }
];
