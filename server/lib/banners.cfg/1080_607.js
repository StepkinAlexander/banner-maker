module.exports = [{
    width: 1080,
    height: 607,
    comment: 'ok, vk',
    logo: {
        top: 60,
        left: 60,
        height: 70,
    },
    ticket: {
        center: {
            top: 306,
            left: 933
        },
        height: 454,
    },
    text: {
        age: {
            fontSize: 44,
            fieldWidth: 100500,
            lineInterval: 0,
            lines: 1,
            top: 62,
            left: 1002,
        },
        title: {
            fontSize: 74,
            fieldWidth: 675,
            lineInterval: 83,
            lines: 2,
            top: 237,
            left: 67,
        },
        subTitle: {
            fontSize: 52,
            fieldWidth: 766,
            lineInterval: 0,
            lines: 1,
            top: 397,
            left: 64,
        },
        footer: {
            fontSize: 15,
            fieldWidth: 560,
            lineInterval: 18,
            lines: 2,
            top: 567,
            left: 62,
        }
    }
}];