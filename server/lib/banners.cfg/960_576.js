module.exports = [{
    width: 960,
    height: 576,
    comment: 'vmet.ro',
    logo: {
        top: 60,
        left: 60,
        height: 70,
    },
    button: {
        top: 397,
        left: 61,
        height: 100,
    },
    ticket: {
        center: {
            top: 289,
            left: 806
        },
        height: 407,
    },
    text: {
        age: {
            fontSize: 34,
            fieldWidth: 100500,
            lineInterval: 0,
            lines: 1,
            top: 48,
            left: 904,
        },
        title: {
            fontSize: 48,
            fieldWidth: 460,
            lineInterval: 56,
            lines: 2,
            top: 218,
            left: 65,
        },

        subTitle: {
            fontSize: 33,
            fieldWidth: 490,
            lineInterval: 0,
            lines: 1,
            top: 324,
            left: 65,
        },
        footer: {
            fontSize: 15,
            fieldWidth: 560,
            lineInterval: 18,
            lines: 2,
            top: 535,
            left: 63,
        }
    }
}];