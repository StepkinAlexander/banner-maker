module.exports = [{
    width: 768,
    height: 1024,
    logo: {
        top: 96,
        left: 242,
        height: 80,
    },
    button: {
        top: 857,
        left: 223,
        height: 93,
    },
    ticket: {
        center: {
            top: 655,
            left: 384
        },
        height: 350,
    },
    close: {
        top: 10,
        left: 733,
        height: 25,
    },
    text: {
        age: {
            fontSize: 26,
            fieldWidth: 100500,
            lineInterval: 0,
            lines: 1,
            top: 34,
            left: 11,
        },
        title: {
            fontSize: 69,
            fieldWidth: 680,
            lineInterval: 72,
            align: 'center',
            lines: 2,
            top: 290,
            left: 0,
        },
        subTitle: {
            fontSize: 41,
            fieldWidth: 640,
            lineInterval: 0,
            align: 'center',
            lines: 1,
            top: 424,
            left: 0,
        },
        footer: {
            fontSize: 17,
            fieldWidth: 628,
            lineInterval: 23,
            align: 'center',
            lines: 2,
            top: 977,
            left: 0,
            shift: true,
        }
    }
}];