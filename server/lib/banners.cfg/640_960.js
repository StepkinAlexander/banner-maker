module.exports = [
    {
        width: 640,
        height: 960,
        comment: 'vmet.ro',
        logo: {
            mobile: false,
            top: 83,
            left: 197,
            height: 70,
        },
        button: {
            top: 755,
            left: 153,
            height: 97,
        },
        ticket: {
            center: {
                top: 568,
                left: 321
            },
            height: 353,
        },
        text: {
            age: {
                fontSize: 34,
                fieldWidth: 100500,
                lineInterval: 0,
                lines: 1,
                top: 39,
                left: 590,
            },
            title: {
                fontSize: 52,
                fieldWidth: 460,
                lineInterval: 56,
                align: 'center',
                lines: 2,
                top: 248,
                left: 0,
            },
            subTitle: {
                fontSize: 30,
                fieldWidth: 500,
                lineInterval: 0,
                align: 'center',
                lines: 1,
                top: 354,
                left: 0,
            },
            footer: {
                fontSize: 15,
                fieldWidth: 560,
                lineInterval: 19,
                align: 'center',
                lines: 2,
                top: 920,
                left: 0,
                shift: true
            }
        }
    },
    {
        width: 320,
        height: 480,
        comment: 'vmet.ro',
        logo: {
            mobile: false,
            top: 41,
            left: 98,
            height: 35,
        },
        button: {
            top: 377,
            left: 76,
            height: 48,
        },
        ticket: {
            center: {
                top: 284,
                left: 160
            },
            height: 176,
        },
        text: {
            age: {
                fontSize: 15,
                fieldWidth: 100500,
                lineInterval: 0,
                lines: 1,
                top: 19,
                left: 297,
            },
            title: {
                fontSize: 26,
                fieldWidth: 320,
                lineInterval: 28,
                align: 'center',
                lines: 2,
                top: 124,
                left: 0,
            },
            subTitle: {
                fontSize: 15,
                fieldWidth: 250,
                lineInterval: 0,
                align: 'center',
                lines: 1,
                top: 177,
                left: 0,
            },
            footer: {
                fontSize: 7,
                fieldWidth: 280,
                lineInterval: 9,
                align: 'center',
                lines: 2,
                top: 460,
                left: 0,
                shift: true
            }
        }
    }
];