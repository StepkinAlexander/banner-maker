module.exports = [{
    width: 320,
    height: 533,
    comment: 'Admob',
    logo: {
        top: 50,
        left: 97,
        height: 35,
    },
    button: {
        top: 424,
        left: 71,
        height: 51,
    },
    ticket: {
        center: {
            top: 327,
            left: 159
        },
        height: 184,
    },
    text: {
        age: {
            fontSize: 11,
            fieldWidth: 100500,
            lineInterval: 0,
            lines: 1,
            top: 13,
            left: 303,
        },
        title: {
            fontSize: 28,
            fieldWidth: 270,
            lineInterval: 36,
            align: 'center',
            lines: 2,
            top: 144,
            left: 0,
        },
        subTitle: {
            fontSize: 18,
            fieldWidth: 270,
            lineInterval: 37,
            align: 'center',
            lines: 1,
            top: 213,
            left: 0,
        },
        footer: {
            fontSize: 10,
            fieldWidth: 307,
            lineInterval: 10,
            align: 'center',
            lines: 2,
            top: 514,
            left: 0,
        }
    }
}];