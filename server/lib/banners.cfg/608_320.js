module.exports = [{
        width: 608,
        height: 320,
        comment: 'vmet.ro [слой 1]',
        logo: {
            top: 439,
            left: 50,
            height: 60,
        },
        button: {
            top: 232,
            left: 50,
            height: 55,
        },
        ticket: {
            center: {
                top: 161,
                left: 484
            },
            height: 238,
        },
        text: {
            age: {
                fontSize: 13,
                fieldWidth: 100500,
                lineInterval: 0,
                lines: 1,
                top: 15,
                left: 588,
            },
            title: {
                fontSize: 33,
                fieldWidth: 325,
                lineInterval: 48,
                lines: 2,
                top: 150,
                left: 49,
            },
        }
    },
    {
        width: 608,
        height: 320,
        comment: 'vmet.ro [слой 2]',
        logo: {
            top: 71,
            left: 180,
            height: 70,
        },
        ticket: {
            center: [
                {
                    top: 161,
                    left: 67
                },
                {
                    top: 161,
                    left: 555
                },
            ],
            height: 160,
        },
        text: {
            age: {
                fontSize: 14,
                fieldWidth: 100500,
                lineInterval: 0,
                lines: 1,
                top: 15,
                left: 588,
            },
            title: {
                fontSize: 38,
                fieldWidth: 360,
                lineInterval: 56,
                align: 'center',
                lines: 2,
                top: 213,
                left: 0,
            },
        }
    }
];