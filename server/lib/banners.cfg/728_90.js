module.exports = [
    {
        width: 728,
        height: 90,
        comment: 'МОРДА ПОИСКА [слой 1]',
        logo: {
            mobile: true,
            top: 15,
            left: 15,
            height: 60,
        },
        ticket: {
            center: {
                top: 47,
                left: 685
            },
            height: 78,
        },
        text: {
            age: {
                fontSize: 15,
                fieldWidth: 100500,
                lineInterval: 0,
                lines: 1,
                top: 23,
                left: 705,
            },
            title: {
                fontSize: 32,
                fieldWidth: 540,
                lineInterval: 32,
                lines: 1,
                top: 45,
                left: 104,
            },
            footer: {
                fontSize: 10,
                fieldWidth: 385,
                lineInterval: 12,
                lines: 2,
                top: 61,
                left: 104,
                shift: true
            }
        }
    },    
    {
        width: 728,
        height: 90,
        comment: 'МОРДА ПОИСКА [слой 2]',
        logo: {
            mobile: true,
            top: 15,
            left: 15,
            height: 60,
        },
        ticket: {
            center: {
                top: 47,
                left: 685
            },
            height: 78,
        },
        text: {
            age: {
                fontSize: 15,
                fieldWidth: 100500,
                lineInterval: 0,
                lines: 1,
                top: 23,
                left: 705,
            },
            title: {
                fontSize: 26,
                fieldWidth: 460,
                lineInterval: 0,
                lines: 1,
                top: 74,
                left: 104,
            },
            logo: {
                fontSize: 26,
                fieldWidth: 400,
                lineInterval: 0,
                lines: 1,
                top: 42,
                left: 103,
            },
        }
    },
    {
        width: 728,
        height: 90,
        comment: 'МОРДА ПОИСКА [слой 3]',
        logo: {
            mobile: true,
            top: 15,
            left: 15,
            height: 60,
        },
        ticket: {
            center: {
                top: 48,
                left: 686
            },
            height: 78,
        },
        text: {
            age: {
                fontSize: 15,
                fieldWidth: 100500,
                lineInterval: 0,
                lines: 1,
                top: 23,
                left: 705,
            },
            footer: {
                fontSize: 15,
                fieldWidth: 375,
                lineInterval: 21,
                lines: 3,
                top: 30,
                left: 102,
                shift: true
            }
        }
    }
];