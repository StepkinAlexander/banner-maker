module.exports = [{
    width: 1200,
    height: 1200,
    comment: 'facebook / instagram',
    logo: {
        top: 160,
        left: 80,
        height: 120,
    },
    ticket: {
        center: {
            top: 578,
            left: 232
        },
        height: 471,
    },
    text: {
        age: {
            fontSize: 24,
            fieldWidth: 100500,
            lineInterval: 0,
            lines: 1,
            top: 40,
            left: 1154,
        },
        title: {
            fontSize: 105,
            fieldWidth: 925,
            lineInterval: 120,
            lines: 2,
            top: 908,
            left: 89,
        },
    }
}];