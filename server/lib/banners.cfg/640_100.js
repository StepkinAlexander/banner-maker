module.exports = [{
        width: 640,
        height: 100,
        comment: ' [слой 1]',
        logo: {
            mobile: true,
            top: 16,
            left: 16,
            height: 68,
        },
        ticket: {
            center: {
                top: 52,
                left: 607
            },
            height: 84,
        },
        text: {
            age: {
                fontSize: 16,
                fieldWidth: 100500,
                lineInterval: 0,
                lines: 1,
                top: 19,
                left: 614,
            },
            title: {
                fontSize: 31,
                fieldWidth: 510,
                lineInterval: 0,
                lines: 1,
                top: 80,
                left: 113,
            },
            logo: {
                fontSize: 31,
                fieldWidth: 460,
                lineInterval: 0,
                lines: 1,
                top: 46,
                left: 112,
            },
        }
    },
    {
        width: 640,
        height: 100,
        comment: ' [слой 2]',
        logo: {
            mobile: true,
            top: 16,
            left: 16,
            height: 68,
        },
        ticket: {
            center: {
                top: 52,
                left: 609
            },
            height: 86,
        },
        text: {
            age: {
                fontSize: 16,
                fieldWidth: 100500,
                lineInterval: 0,
                lines: 1,
                top: 19,
                left: 614,
            },
            footer: {
                fontSize: 15,
                fieldWidth: 380,
                lineInterval: 20,
                lines: 3,
                top: 36,
                left: 110,
                shift: true,
            }
        }
    }
];