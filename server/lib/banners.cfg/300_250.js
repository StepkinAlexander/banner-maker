module.exports = [{
    width: 300,
    height: 250,
    comment: 'Admob',
    logo: {
        top: 27,
        left: 26,
        height: 28,
    },
    button: {
        top: 181,
        left: 25,
        height: 34,
    },
    ticket: {
        center: {
            top: 215,
            left: 245
        },
        height: 128,
    },
    bg: {
        center: {
            top: 250,
            left: 200,
        }
    },
    text: {
        age: {
            fontSize: 11,
            fieldWidth: 100500,
            lineInterval: 0,
            lines: 1,
            top: 13,
            left: 283,
        },
        title: {
            fontSize: 22,
            fieldWidth: 220,
            lineInterval: 27,
            lines: 2,
            top: 94,
            left: 26,
        },
        subTitle: {
            fontSize: 15,
            fieldWidth: 230,
            lineInterval: 0,
            lines: 1,
            top: 146,
            left: 25,
        },
        footer: {
            fontSize: 8,
            fieldWidth: 230,
            lineInterval: 11,
            lines: 2,
            top: 230,
            left: 26,
        }
    }
}];