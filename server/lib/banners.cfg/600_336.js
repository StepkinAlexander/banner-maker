module.exports = [{
    width: 600,
    height: 336,
    comment: '',
    logo: {
        top: 36,
        left: 35,
        height: 40,
    },
    button: {
        top: 244,
        left: 35,
        height: 55,
    },
    ticket: {
        center: {
            top: 169,
            left: 479
        },
        height: 234,
    },
    text: {
        age: {
            fontSize: 15,
            fieldWidth: 100500,
            lineInterval: 0,
            lines: 1,
            top: 16,
            left: 576,
        },
        title: {
            fontSize: 33,
            fieldWidth: 330,
            lineInterval: 47,
            lines: 2,
            top: 129,
            left: 34,
        },
        subTitle: {
            fontSize: 20,
            fieldWidth: 330,
            lineInterval: 0,
            lines: 1,
            top: 211,
            left: 35,
        },
        footer: {
            fontSize: 10,
            fieldWidth: 380,
            lineInterval: 12,
            lines: 2,
            top: 313,
            left: 35,
        }
    }
}];