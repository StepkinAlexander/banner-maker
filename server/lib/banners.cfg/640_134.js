module.exports = [{
        width: 640,
        height: 134,
        comment: 'Главная Яндекс без кнопки 1px рамка [слой 1]',
        border: 1,
        logo: {
            mobile: true,
            top: 21,
            left: 21,
            height: 92,
        },
        ticket: {
            center: {
                top: 117,
                left: 585
            },
            height: 90,
        },
        text: {
            age: {
                fontSize: 14,
                fieldWidth: 100500,
                lineInterval: 0,
                lines: 1,
                top: 16,
                left: 619,
            },
            title: {
                fontSize: 28,
                fieldWidth: 480,
                lineInterval: 0,
                lines: 1,
                top: 60,
                left: 137,
            },
            footer: {
                fontSize: 10,
                fieldWidth: 385,
                lineInterval: 10,
                lines: 2,
                top: 84,
                left: 135,
            }
        }
    },
    {
        width: 640,
        height: 134,
        comment: 'Главная Яндекс без кнопки 1px рамка [слой 2]',
        border: 1,
        logo: {
            mobile: true,
            top: 21,
            left: 21,
            height: 92,
        },
        text: {
            age: {
                fontSize: 14,
                fieldWidth: 100500,
                lineInterval: 0,
                lines: 1,
                top: 16,
                left: 619,
            },
            title: {
                fontSize: 28,
                fieldWidth: 480,
                lineInterval: 0,
                lines: 1,
                top: 92,
                left: 137,
            },
            logo: {
                fontSize: 29,
                fieldWidth: 435,
                lineInterval: 0,
                lines: 1,
                top: 54,
                left: 135,
            },
        }
    },
    {
        width: 640,
        height: 134,
        comment: 'Главная Яндекс без кнопки 1px рамка [слой 3]',
        border: 1,
        logo: {
            mobile: true,
            top: 21,
            left: 21,
            height: 92,
        },
        text: {
            age: {
                fontSize: 13,
                fieldWidth: 100500,
                lineInterval: 0,
                lines: 1,
                top: 14,
                left: 623,
            },
            footer: {
                fontSize: 19,
                fieldWidth: 460,
                lineInterval: 25,
                lines: 3,
                top: 49,
                left: 135,
                shift: true,
            }
        }
    }
];