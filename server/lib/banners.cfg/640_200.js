module.exports = [{
        width: 640,
        height: 200,
        comment: 'vmet.ro [слой 1]',
        logo: {
            mobile: true,
            top: 40,
            left: 40,
            height: 120,
        },
        ticket: {
            center: {
                top: 102,
                left: 593
            },
            height: 172,
        },
        text: {
            age: {
                fontSize: 13,
                fieldWidth: 100500,
                lineInterval: 0,
                lines: 1,
                top: 14,
                left: 620,
            },
            title: {
                fontSize: 35,
                fieldWidth: 340,
                lineInterval: 40,
                lines: 2,
                top: 114,
                left: 194,
            },
            logo: {
                fontSize: 36,
                fieldWidth: 340,
                lineInterval: 36,
                lines: 1,
                top: 75,
                left: 192,
            },
        }
    },
    {
        width: 640,
        height: 200,
        comment: 'vmet.ro [слой 2]',
        logo: {
            mobile: true,
            top: 40,
            left: 40,
            height: 120,
        },
        ticket: {
            center: {
                top: 102,
                left: 594
            },
            height: 170,
        },
        text: {
            age: {
                fontSize: 13,
                fieldWidth: 100500,
                lineInterval: 0,
                lines: 1,
                top: 14,
                left: 620,
            },
            footer: {
                fontSize: 20,
                fieldWidth: 400,
                lineInterval: 30,
                lines: 3,
                top: 78,
                left: 192,
                shift: true,
                dashshift: true,
            }
        }
    }
];