module.exports = [{
        width: 970,
        height: 90,
        comment: ' [слой 1]',
        logo: {
            mobile: true,
            top: 15,
            left: 15,
            height: 60,
        },
        ticket: {
            center: {
                top: 45,
                left: 880
            },
            height: 76,
        },
        text: {
            age: {
                fontSize: 10,
                fieldWidth: 100500,
                lineInterval: 0,
                lines: 1,
                top: 13,
                left: 953,
            },
            title: {
                fontSize: 30,
                fieldWidth: 505,
                lineInterval: 0,
                lines: 1,
                top: 72,
                left: 103,
            },
            logo: {
                fontSize: 30,
                fieldWidth: 245,
                lineInterval: 0,
                lines: 1,
                top: 43,
                left: 102,
            },
        }
    },
    {
        width: 970,
        height: 90,
        comment: ' [слой 2]',
        logo: {
            mobile: true,
            top: 15,
            left: 10,
            height: 60,
        },
        ticket: {
            center: {
                top: 45,
                left: 881
            },
            height: 76,
        },
        text: {
            age: {
                fontSize: 10,
                fieldWidth: 100500,
                lineInterval: 0,
                lines: 1,
                top: 13,
                left: 953,
            },
            footer: {
                fontSize: 21,
                fieldWidth: 765,
                lineInterval: 31,
                lines: 2,
                top: 39,
                left: 97,
            }
        }
    }
];