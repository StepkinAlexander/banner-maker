module.exports = [{
        width: 320,
        height: 100,
        comment: 'vmet.ro [слой 1]',
        logo: {
            mobile: true,
            top: 20,
            left: 20,
            height: 60,
        },
        ticket: {
            center: {
                top: 51,
                left: 295
            },
            height: 86,
        },
        text: {
            age: {
                fontSize: 7,
                fieldWidth: 100500,
                lineInterval: 0,
                lines: 1,
                top: 9,
                left: 308,
            },
            title: {
                fontSize: 15,
                fieldWidth: 135,
                lineInterval: 20,
                lines: 2,
                top: 57,
                left: 97,
            },
            logo: {
                fontSize: 18,
                fieldWidth: 230,
                lineInterval: 18,
                lines: 1,
                top: 37,
                left: 96,
            },
        }
    },
    {
        width: 320,
        height: 100,
        comment: 'vmet.ro [слой 2]',
        logo: {
            mobile: true,
            top: 20,
            left: 20,
            height: 60,
        },
        ticket: {
            center: {
                top: 51,
                left: 295
            },
            height: 86,
        },
        text: {
            age: {
                fontSize: 7,
                fieldWidth: 100500,
                lineInterval: 0,
                lines: 1,
                top: 9,
                left: 308,
            },
            footer: {
                fontSize: 10,
                fieldWidth: 200,
                lineInterval: 15,
                lines: 3,
                top: 39,
                left: 96,
                shift: true,
                dashshift: true,
            }
        }
    }
];