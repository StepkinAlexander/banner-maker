module.exports = [{
    width: 1080,
    height: 1920,
    logo: {
        top: 140,
        left: 310,
        height: 131,
    },
    button: {
        top: 1540,
        left: 202,
        height: 197,
    },
    ticket: {
        center: {
            top: 1125,
            left: 544
        },
        height: 773,
    },
    close: {
        top: 22,
        left: 1010,
        height: 48,
    },
    text: {
        age: {
            fontSize: 26,
            fieldWidth: 100500,
            fontWidth: 1,
            lineInterval: 0,
            lines: 1,
            top: 52,
            left: 21,
        },
        title: {
            fontSize: 82,
            fieldWidth: 784,
            lineInterval: 110,
            align: 'center',
            lines: 2,
            top: 443,
            left: 0,
        },
        subTitle: {
            fontSize: 54,
            fieldWidth: 837,
            lineInterval: 0,
            align: 'center',
            lines: 1,
            top: 647,
            left: 0,
            shift: true,
        },
        footer: {
            fontSize: 29,
            fieldWidth: 1040,
            lineInterval: 33,
            align: 'center',
            lines: 2,
            top: 1844,
            left: 0,
        }
    }
}];