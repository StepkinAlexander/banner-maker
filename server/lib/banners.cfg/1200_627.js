module.exports = [{
    width: 1200,
    height: 627,
    comment: 'facebook',
    logo: {
        top: 141,
        left: 80,
        height: 90,
    },
    ticket: {
        center: {
            top: 315,
            left: 1041
        },
        height: 429,
    },
    text: {
        age: {
            fontSize: 25,
            fieldWidth: 100500,
            lineInterval: 0,
            lines: 1,
            top: 38,
            left: 1158,
        },
        title: {
            fontSize: 92,
            fieldWidth: 820,
            lineInterval: 89,
            lines: 2,
            top: 408,
            left: 88,
        },
    }
}];