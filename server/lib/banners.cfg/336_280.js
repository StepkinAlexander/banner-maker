module.exports = [{
    width: 336,
    height: 280,
    comment: 'Admob',
    logo: {
        top: 25,
        left: 25,
        height: 30,
    },
    button: {
        top: 202,
        left: 25,
        height: 40,
    },
    ticket: {
        center: {
            top: 216,
            left: 253
        },
        height: 164,
    },
    text: {
        age: {
            fontSize: 10,
            fieldWidth: 100500,
            lineInterval: 0,
            lines: 1,
            top: 12,
            left: 320,
        },
        title: {
            fontSize: 24,
            fieldWidth: 220,
            lineInterval: 30,
            lines: 2,
            top: 93,
            left: 26,
        },
        subTitle: {
            fontSize: 15,
            fieldWidth: 223,
            lineInterval: 0,
            lines: 1,
            top: 148,
            left: 26,
        },
        footer: {
            fontSize: 10,
            fieldWidth: 285,
            lineInterval: 12,
            lines: 2,
            top: 256,
            left: 26,
        }
    }
}];