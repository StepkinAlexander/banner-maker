module.exports = [{
    width: 1536,
    height: 2048,
    logo: {
        top: 192,
        left: 485,
        height: 160,
    },
    button: {
        top: 1715,
        left: 446,
        height: 186,
    },
    ticket: {
        center: {
            top: 1311,
            left: 769
        },
        height: 700,
    },
    close: {
        top: 20,
        left: 1466,
        height: 50,
    },
    text: {
        age: {
            fontSize: 45,
            fieldWidth: 100500,
            lineInterval: 0,
            lines: 1,
            top: 69,
            left: 23,
        },
        title: {
            fontSize: 138,
            fieldWidth: 1360,
            lineInterval: 145,
            align: 'center',
            lines: 2,
            top: 581,
            left: 0,
        },
        subTitle: {
            fontSize: 82,
            fieldWidth: 1280,
            lineInterval: 0,
            align: 'center',
            lines: 1,
            top: 849,
            left: 0,
        },
        footer: {
            fontSize: 33,
            fieldWidth: 1256,
            lineInterval: 38,
            align: 'center',
            lines: 2,
            top: 1954,
            left: 0,
            shift: true,
        }
    }
}];