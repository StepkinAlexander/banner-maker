module.exports = [{
        width: 468,
        height: 60,
        comment: 'Admob [слой 1]',
        logo: {
            mobile: true,
            top: 13,
            left: 13,
            height: 34,
        },
        ticket: {
            center: {
                top: 30,
                left: 434
            },
            height: 52,
        },
        text: {
            age: {
                fontSize: 9,
                fieldWidth: 100500,
                lineInterval: 0,
                lines: 1,
                top: 10,
                left: 454,
            },
            title: {
                fontSize: 18,
                fieldWidth: 305,
                lineInterval: 18,
                lines: 1,
                top: 48,
                left: 58,
            },
            logo: {
                fontSize: 18,
                fieldWidth: 245,
                lineInterval: 0,
                lines: 1,
                top: 29,
                left: 58,
            },
        }
    },
    {
        width: 468,
        height: 60,
        comment: 'Admob [слой 2]',
        logo: {
            mobile: true,
            top: 13,
            left: 13,
            height: 34,
        },
        ticket: {
            center: {
                top: 30,
                left: 435
            },
            height: 53,
        },
        text: {
            age: {
                fontSize: 9,
                fieldWidth: 100500,
                lineInterval: 0,
                lines: 1,
                top: 10,
                left: 449,
            },
            footer: {
                fontSize: 12,
                fieldWidth: 362,
                lineInterval: 14,
                lines: 2,
                top: 27,
                left: 64,
            }
        }
    }
];