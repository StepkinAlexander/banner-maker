module.exports = [{
    width: 1920,
    height: 1080,
    logo: {
        top: 120,
        left: 120,
        height: 150,
    },
    button: {
        top: 758,
        left: 120,
        height: 151,
    },
    ticket: {
        center: {
            top: 542,
            left: 1633
        },
        height: 750,
    },
    close: {
        top: 21,
        left: 1852,
        height: 50,
    },
    text: {
        age: {
            fontSize: 45,
            fieldWidth: 100500,
            lineInterval: 0,
            lines: 1,
            top: 68,
            left: 23,
        },
        title: {
            fontSize: 105,
            fieldWidth: 920,
            lineInterval: 110,
            lines: 2,
            top: 437,
            left: 129,
        },
        subTitle: {
            fontSize: 78,
            fieldWidth: 1140,
            lineInterval: 0,
            lines: 1,
            top: 648,
            left: 124,
        },
        footer: {
            fontSize: 34,
            fieldWidth: 1250,
            lineInterval: 38,
            lines: 2,
            top: 990,
            left: 122,
            shift: true,
        }
    }
}];
