module.exports = [{
    width: num,
    height: num,
    comment: string, // string comment under the banner look like 'widthXheght comment'
    border: num, //px border line if exist or no border
    logo: { // задаёт отображение логотипа на баннере, задаётся координатами левого верхнего угла и высоты элемента
        visible: boolean, // пропустить элемент можно пропустив весь раздел или выставить это значение в false
        top: num, // or not present if not visible // топ изобрежения - расстояние от верхней границы баннера до верхней границы изображения
        left: num, // or not present if not visible
        height: num, // or not present if not visible
    },
    button: { // задаёт отображение кнопки скачать на баннере
        visible: boolean, // пропустить элемент можно пропустив весь раздел или выставить это значение в false
        top: num, // or not present if not visible
        left: num, // or not present if not visible
        height: num, // or not present if not visible
    },
    bg: { // задаёт положение центра фона относительно левого верхнего угла баннера (раздел можно пропустить)
        center: {
            top: num,
            left: num,
        }
    },
    ticket: { // задаёт отображение графического элемента на странице координатами центра относительно левого верхнего угла баннера и высотой элемента
        visible: boolean, // пропустить элемент можно пропустив весь раздел или выставить это значение в false
        center: { // раздел можно пропустить
            top: num, // or not present if not visible
            left: num, // or not present if not visible
        },
        height: num, // or not present if not visible
    },
    close: { // задаёт отображение кнопки закрытия на странице координатами центра относительно левого верхнего угла баннера и высотой элемента
        visible: boolean, // пропустить элемент можно пропустив весь раздел или выставить это значение в false
        top: num, // or not present if not visible
        left: num, // or not present if not visible
        height: num, // or not present if not visible
    },
    text: { // тексты баннера
        age: { // возрастной ценз
            visible: boolean, // пропустить элемент можно пропустив весь раздел или выставить это значение в false
            fontSize:  num, // or not present if not visible
            fieldWidth:  num, // or not present if not visible or big num(e.g. 100500) for not limited line
            fontWidth:  num, // or not present if not visible
            lineInterval:  num, // or not present if not visible
            align: string, // value: 'center' or not present if not centered
            lines:  num, // or not present if not visible
            top: num, // or not present if not visible // топ для текстов считается расстоянием от верхней границы баннера до низа текста
            left: num, // or not present if not visible
            shift: boolean, //учитывать перенос, в поле текста перенос выделяется "\n"
            dashshift: boolean, //учитывать перенос также по тире
        },
        logo: { // текст-логотип
            //аналогично text.age
        },
        title: { // заголовок баннера
            //аналогично text.age
        },
        subTitle: { // подзаголовок
            //аналогично text.age
        },
        footer: { // подвал
            //аналогично text.age
        }
    }
}];