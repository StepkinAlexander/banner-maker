module.exports = [{
    width: 1024,
    height: 768,
    logo: {
        top: 75,
        left: 60,
        height: 85,
    },
    button: {
        top: 575,
        left: 60,
        height: 99,
    },
    ticket: {
        center: {
            top: 385,
            left: 852
        },
        height: 466,
    },
    close: {
        top: 10,
        left: 989,
        height: 25,
    },
    text: {
        age: {
            fontSize: 28,
            fieldWidth: 100500,
            lineInterval: 0,
            lines: 1,
            top: 34,
            left: 11,
        },
        title: {
            fontSize: 68,
            fieldWidth: 615,
            lineInterval: 71,
            lines: 2,
            top: 269,
            left: 65,
        },
        subTitle: {
            fontSize: 40,
            fieldWidth: 600,
            lineInterval: 0,
            lines: 1,
            top: 396,
            left: 62,
        },
        footer: {
            fontSize: 18,
            fieldWidth: 660,
            lineInterval: 19,
            lines: 2,
            top: 726,
            left: 61,
        }
    }
}];