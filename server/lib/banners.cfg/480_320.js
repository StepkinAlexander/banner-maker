module.exports = [{
    width: 480,
    height: 320,
    comment: 'Admob full screen',
    logo: {
        top: 35,
        left: 35,
        height: 34,
    },
    button: {
        top: 234,
        left: 33,
        height: 47,
    },
    ticket: {
        center: {
            top: 161,
            left: 408
        },
        height: 188,
    },
    text: {
        age: {
            fontSize: 11,
            fieldWidth: 100500,
            lineInterval: 0,
            lines: 1,
            top: 13,
            left: 463,
        },
        title: {
            fontSize: 32,
            fieldWidth: 290,
            lineInterval: 43,
            lines: 2,
            top: 120,
            left: 32,
        },
        subTitle: {
            fontSize: 20,
            fieldWidth: 300,
            lineInterval: 0,
            lines: 1,
            top: 194,
            left: 33,
        },
        footer: {
            fontSize: 10,
            fieldWidth: 442,
            lineInterval: 12,
            lines: 2,
            top: 295,
            left: 34,
        }
    }
}];