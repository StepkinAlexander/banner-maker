module.exports = [{
    width: 800,
    height: 320,
    comment: 'twitter Avito',
    logo: {
        top: 44,
        left: 45,
        height: 64,
    },
    ticket: {
        center: {
            top: 162,
            left: 691
        },
        height: 260,
    },
    text: {
        age: {
            fontSize: 28,
            fieldWidth: 100500,
            lineInterval: 0,
            lines: 1,
            top: 35,
            left: 758,
        },
        title: {
            fontSize: 48,
            fieldWidth: 435,
            lineInterval: 56,
            lines: 2,
            top: 192,
            left: 50,
        },
    }
}];