module.exports = [{
        width: 320,
        height: 50,
        comment: ' [слой 1]',
        logo: {
            mobile: true,
            top: 8,
            left: 8,
            height: 34,
        },
        ticket: {
            center: {
                top: 26,
                left: 303
            },
            height: 42,
        },
        text: {
            age: {
                fontSize: 8,
                fieldWidth: 100500,
                lineInterval: 0,
                lines: 1,
                top: 9,
                left: 307,
            },
            title: {
                fontSize: 15,
                fieldWidth: 255,
                lineInterval: 10,
                lines: 1,
                top: 40,
                left: 56,
            },
            logo: {
                fontSize: 15,
                fieldWidth: 230,
                lineInterval: 0,
                lines: 1,
                top: 23,
                left: 56,
            },
        }
    },
    {
        width: 320,
        height: 50,
        comment: ' [слой 2]',
        logo: {
            mobile: true,
            top: 8,
            left: 8,
            height: 34,
        },
        ticket: {
            center: {
                top: 27,
                left: 304
            },
            height: 42,
        },
        text: {
            age: {
                fontSize: 8,
                fieldWidth: 100500,
                lineInterval: 0,
                lines: 1,
                top: 9,
                left: 307,
            },
            footer: {
                fontSize: 7,
                fieldWidth: 177,
                lineInterval: 12,
                lines: 3,
                top: 16,
                left: 55,
                shift: true,
            }
        }
    }
];