module.exports = [{
    width: 480,
    height: 32,
    comment: ' OK',
    logo: {
        mobile: true,
        top: 4,
        left: 7,
        height: 24,
    },
    ticket: {
        center: {
            top: 25,
            left: 443
        },
        height: 44,
    },
    text: {
        age: {
            fontSize: 8,
            fieldWidth: 100500,
            fontWidth: 5,
            lineInterval: 0,
            lines: 1,
            top: 10,
            left: 466,
        },
        title: {
            fontSize: 15,
            fieldWidth: 245,
            lineInterval: 15,
            lines: 1,
            top: 22,
            left: 148,
        },
        logo: {
            fontSize: 15,
            fieldWidth: 245,
            lineInterval: 0,
            lines: 1,
            top: 22,
            left: 39,
        },
    }
}];