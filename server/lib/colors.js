'use strict'
var path = require('path');

var getFontColor = function (bgschema) {
        return (['yellow', 'white', 'photo_light'].indexOf(bgschema) != -1) ? 'black' : 'white';
    }

exports.getFontColor = getFontColor;

var getAgeFontColor = function (bgschema) {
    var color;
        if (bgschema == 'black') {
            color = 'rgba(255,255,255,0.6)';
        } else if (bgschema == 'photo_dark') {
            color = 'rgba(255,255,255,0.8)';
        } else {
            color = 'rgba(0, 0, 0, 0.6)';
        }
        return color;
    }
exports.getAgeFontColor = getAgeFontColor;

var getFooterFontColor = function (bgschema) {
    var color;
        if (bgschema == 'black') {
            color = 'rgba(255,255,255,0.8)';
        } else if (bgschema == 'photo_dark') {
            color = 'rgba(255,255,255,0.8)';
        } else {
            color = 'rgba(0, 0, 0, 0.6)';
        }
        return color;
    }
exports.getFooterFontColor = getFooterFontColor;

var getBackgroundColor = function (colorSchema, width, height) {
        var bk = {
            white: 0xFFFFFFFF,
            black: 0x0F0F0FFF,
            yellow: 0xFFCC00FF,
        }[colorSchema];
        if(bk==undefined) bk = 0xFFFFFFFF;

        if('yellow'==colorSchema && 640==parseInt(width) && 134==parseInt(height)) {
            bk = 0xFFE98CFF;
        }
        return bk;
    }
exports.getBackgroundColor = getBackgroundColor;
