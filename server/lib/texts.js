const utils = require('./fontutil');

var getSplittedLabelList = function (label, params, shift) {
        if(!params.shift)
            label = label.replace(/\\n/g, '')
        else
            label = label.replace(/\\n/g, '\n ');

        if(!!params.dashshift)
            label = label.replace(/\-/g, '- ');

        label = label.replace(/  /g,' ');
        var bgschema = params.bgschema,
            fieldWidth = params.fieldWidth,
            fontSize = params.fontSize;

        if (utils.getTextWidth(label,fontSize) > fieldWidth) {
            var labelArr = label.split(' ');
            var t1 = [], t2 = '', t1done = false;
            for(var i=0; i<labelArr.length; i++) {
                if(labelArr[i].match(/\n/) && !t1done) {
                    labelArr[i] = labelArr[i].replace(/\n/, '');
                    t1.push(labelArr[i]);
                    t1done = t1.length;
                } else if(utils.getTextWidth(t1.join(' '),fontSize) <= fieldWidth && !t1done) {
                    t1.push(labelArr[i]);
                };

                if(utils.getTextWidth(t1.join(' '),fontSize) > fieldWidth) {
                    t1 = t1.slice(0, t1.length-1);
                    t1done = t1.length;
                }
            };

            //висячие предлоги fix
            var last = t1[t1.length-1];
            if(last.length<3 || ['под', 'над', 'около', 'при', 'перед'].indexOf(last) != -1) {
                t1 = t1.slice(0, t1.length-1);
                t1done = t1.length;
            }
            //end fix

            if(!!t1done)
                t2 = getSplittedLabelList(labelArr.slice(t1done).join(' '), params);

            return [t1.join(' ')].concat(t2);
        } else {
            return [label];
        };
    }
exports.getSplittedLabelList = getSplittedLabelList;
