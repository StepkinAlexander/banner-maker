'use strict';
const config = require('cfg');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const express = require('express');
const expressBundleResponse = require('express-bundle-response');
const expressTld = require('express-tld');

const app = express();
const zip = require('express-zip');

const fs = require('fs');
const path = require('path');
const bannersDir = path.join(__dirname, '../', config.bannersTmp);
if (!fs.existsSync(bannersDir)){
    fs.mkdirSync(bannersDir);
}

const csrf = require('csrf-lite');

app.set('env', config.environment);
app.disable('x-powered-by');
app.disable('etag');

app.enable('trust proxy');

app.use('/static', express.static(config.static.dir, {
    fallthrough: false,
    maxAge: 365 * 24 * 60 * 60 * 1000
}));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(function (req, res, next) {
    if (req.cookies.csrf_token) {
        req.csrfToken = req.cookies.csrf_token
    } else {
        req.csrfToken = csrf()
        res.cookie('csrf_token', req.csrfToken, {path: '/'});
    }
    next();
});


app.use(expressTld());
app.use(expressBundleResponse(config.view));

require('./router')(app);

module.exports = app;
