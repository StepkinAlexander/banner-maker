'use strict';
const multer = require('multer');
const upload = multer();

module.exports = function Router(app) {
    app.get('/', require('./controllers/index').index);
    app.post('/generate', upload.any(), require('./controllers/index').generate);
    app.get('/log', require('./controllers/index').log);
    app.get('/download', require('./controllers/index').zip);
};
