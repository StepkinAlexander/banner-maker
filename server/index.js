'use strict';
const app = require('./app.js');
const config = require('cfg');

app.listen(config.server.port, err => {
    if (err) {
        console.error('Server startup failed:');
        console.error(err);
        return;
    }

    console.log(`Server started on http://${config.server.hostname}:${config.server.port}/`);
});
