'use strict';
const config = require('cfg');
const path = require('path');
const fs = require('fs');

const logger = require('../lib/logger');

var LRU = require("lru-cache"),
    formsCache = LRU({
        maxAge: 3600000,
    });

require('string-format').extend(String.prototype);

const Promise = require('es6-promise').Promise;
const bannerLib = require('../lib/banner');

const bannersDir = path.join(__dirname, '../../', require('cfg').bannersTmp);

const availableScales = [
        '1242_360',
        '300_250',
        '320_480',
        '320_50_admob',
        '360_50',
        '320_533',
        '468_60',
        '970_90',
        '336_280',
        '480_32',
        '480_320',
        '600_336',
        '608_320',
        '320_50',
        '640_100',
        '640_134',
        '320_100',
        '640_200',
        '728_90',
        '640_960',
        '800_320',
        '960_576',
        '960_640',
        '1080_607',
        '1080_1920',
        '1155_400',
        '1200_627',
        '1200_1200',
        '1536_2048',
        '1920_1080',
        '768_1024',
        '1024_768',
        '2048_1536',
    ];

module.exports = { 
    index: (req, res, next) => {
        const data = {
            version: 'config.app.version',
            staticHost: config.static.host,
            debug: 'config.app.debug',
            host: req.headers.host,
            language: 'ru',
            contentRegion: req.tld.split('.').pop(),
            tld: req.tld,
            uatraits: req.uatraits,
            nonce: req.nonce,
            bunker: req.bunker
        };

        res.bundle.render('index', data);
    },

    generate: (req, res,next) => {
        logger.info('request /generate')

        var cacheKey = req.body.csrf_token;
        var formValues;
        if(req.body.new_data) {
            formsCache.set(cacheKey, {
                body: req.body,
                files: req.files,
            })
        };
        formValues = formsCache.get(cacheKey);

        var params = {
                ageplus: formValues.body.ageplus,
                logotitle: formValues.body.logotitle,
                title: formValues.body.title,
                subtitle: formValues.body.subtitle,
                button: formValues.body.button || false,
                footer: formValues.body.footer,
                bgschema: formValues.body.bgschema || 'photo_light',
                btntype: formValues.body.btntype || 'ydownload',
                csrf: formValues.body.csrf_token,
            },
            formImages = {};
        var scale = req.query.scale;
        if (availableScales.indexOf(scale) == -1) {
            res.bundle.render('errors', {
                format: scale,
                error: 'Генерация подобного слайда не реализована',
            });
            return
        };
        params.scale = scale;

        var slide = ([0,1,2].indexOf(parseInt(req.query.banner)) != -1) ? parseInt(req.query.banner) : 0;
        params.slide = slide;

        if(formValues.files) {
            if (!!(formValues.files.length | (formValues.files[0] | []).length | ((formValues.files[0] | []).file | []).length)) {
                var fi;
                if (formValues.files[0]) {
                    fi = formValues.files;
                } else {
                    fi = formValues.files.file;
                };

                var formImages = {};
                for(var f=0;f<fi.length;f++) {
                    formImages[fi[f].fieldname] = fi[f].buffer;
                };
            };
        };

        var startTime = Date.now()
        Promise.all([
            bannerLib.getBanner(params, formImages, require('../lib/banners.cfg/{0}'.format(scale))[slide]),
        ])
        .then(function(bannerList) {
            logger.info('generated {0} banners at {1} seconds'.format(bannerList.length, ((Date.now()-startTime)/1000).toFixed(2)))

            res.bundle.render('banners', bannerList);
        })
        .catch(function(err) {
            logger.info(err.stack)
            res.status(200).send(err.stack);
        });
    },

    log: (req, res, next) => {
        var run_cmd = function (cmd, args, callBack ) {
            var spawn = require('child_process').spawn;
            var child = spawn(cmd, args);
            var resp = "";

            child.stdout.on('data', function (buffer) {
                resp += buffer.toString().replace(/\n/g,'<br>');
            });
            child.stdout.on('end', function() { callBack (resp) });
        };

        run_cmd('tail', ['-n', '150', path.join(config.logger.dir, config.logger.filename)], function(resp) {
            res.status(200).send(resp);
        });
    },

    zip: (req, res, next) => {
        var userDir = path.join(bannersDir, req.csrfToken.replace(/\W/g,''));

        fs.readdir(userDir, function(err, files) {
            if(err) {
                next(err);
                return;
            };

            var fileList = [];
            files
            .forEach(function(filename) {
                fileList.push({
                    path: path.join(userDir, filename),
                    name: filename
                })
            });

            res.zip(fileList)
        });
    }
};
