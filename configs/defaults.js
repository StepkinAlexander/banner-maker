'use strict';
const os = require('os');

module.exports = {
    environment: 'prod',

    server: {
        port: 8000,
        hostname: os.hostname()
    },

    static: {
        host: '/static',
        dir: __dirname + '/../static'
    },

    app: {
        domains: 'ru'
    },

    view: {
        templateRoot: __dirname + '/../static',
        block: 'b-page',
        templateExt: 'bh',
        cwd: __dirname + '/../static',
        detect: (bundle, req) =>  bundle
            .type('desktop')
    },

    logger: {
        dir: __dirname + '/../',
        filename: 'maker.log',
    },

    bannersTmp: 'banners-tmp',
};
