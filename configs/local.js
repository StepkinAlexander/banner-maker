'use strict';

module.exports = {
    environment: 'development',

    view: {
        dev: true
    },
};
