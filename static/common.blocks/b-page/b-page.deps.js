/* eslint-disable no-unused-expressions */
({
    mustDeps: [
        {
            block: 'i-bem',
            elem: 'dom',
            mods: { init: 'auto' }
        },
        {
            block: 'i-bem',
            elem: 'i18n'
        },
        {
            block: 'i-global'
        },
        {
            block: 'i-services'
        },
        {
            block: 'b-foot'
        },
        {
            block: 'logo',
            mods: { lang: 'ru' }
        }
    ],
    shouldDeps: [
        { block: 'b-form' },
        { block: 'b-banner' },
        { block: 'radiobox', mods: { size: 's', 'flow': 'vertical' }, elems: [ 'radio', 'box', 'control' ] },
        { block: 'i-font', mods: { face: 'textbook-new-regular' } },
        { block: 'i-font', mods: { face: 'textbook-new-light' } },
    ]
});
