module.exports = function (blocks) {
    'use strict';
    blocks.declare('b-page', (data, page) => [
        {
            block: 'i-global',
            params: {
                lang: data.language,
                tld: data.tld,
                contentRegion: data.contentRegion,
                secretKey: data.secretkey,
                retpath: data.url,
                host: `https://${data.host}`,
                nonce: data.nonce,
                id: ''
            }
        },
        {
            block: 'b-page',
            title: 'Banner maker 2',
            head: [
                { elem: 'css', url: `${data.staticHost}/desktop.bundles/${page}/_${page}.css`, ie: false },
                { elem: 'css', url: `${data.staticHost}/desktop.bundles/${page}/_${page}`, ie: true },
                {
                    elem: 'cc',
                    condition: 'IE 8',
                    content: { elem: 'js', url: 'https://yastatic.net/es5-shims/0.0.2/es5-shims.min.js' }
                },
                { elem: 'js', url: 'https://yastatic.net/jquery/1.12.0/jquery.min.js' },
                {
                    elem: 'js',
                    url: `${data.staticHost}/desktop.bundles/${page}/_${page}.${data.language}.js`
                },

                // Если нужны поделяшки. https://github.yandex-team.ru/share/share2
                // { elem: 'js', url: 'https://yastatic.net/share2/share.js', attrs: { async: 'async' }}

                // FIXME: не забываем заменить favicon на иконку своего сервиса
                { elem: 'favicon', url: 'https://yastatic.net/lego/_/pDu9OWAQKB0s2J9IojKpiS_Eho.ico' },

                // FIXME: Меты для поисковой оптимизации
                { elem: 'meta', attrs: { name: 'description', content: '' }},
                { elem: 'meta', attrs: { name: 'keywords', content: '' }},

                // FIXME: Меты OpenGraph протокола http://developers.facebook.com/docs/opengraph/
                { elem: 'meta', attrs: { property: 'og:title', content: '' }},
                { elem: 'meta', attrs: { property: 'og:description', content: '' }},
                { elem: 'meta', attrs: { property: 'og:image', content: '' }},
                { elem: 'meta', attrs: { property: 'og:type', content: 'website' }}
            ],
            xUaCompatible: 'IE=edge',
            content: [
                { block: 'b-form', tag: 'form', attrs: { action: '.', method: 'post' }, js: true, content: [
                    { elem: 'container', content: [
                        { elem: 'form', content: [
                            { elem: 'text-inputs', content: [
                                { elem: 'field-block', content: [
                                    { elem: 'field-title', content: 'Возрастная категория' },
                                    { elem: 'field-input', tag: 'input', attrs: { type: 'text', name: 'ageplus', value: '0+' } },
                                ] },
                                { elem: 'field-block', content: [
                                    { elem: 'field-title', content: 'Кастомный текст Лого' },
                                    { elem: 'field-input', tag: 'input', attrs: { type: 'text', name: 'logotitle', value: 'Яндекс.Такси' } },
                                ] },
                                { elem: 'field-block', content: [
                                    { elem: 'field-title', content: 'Заголовок' },
                                    { elem: 'field-input', tag: 'input', attrs: { type: 'text', name: 'title', value: 'Плати наличными\n в своем городе' } },
                                ] },
                                { elem: 'field-block', content: [
                                    { elem: 'field-title', content: 'Подзаголовок' },
                                    { elem: 'field-input', tag: 'input', attrs: { type: 'text', name: 'subtitle', value: 'Очки виртуальной реальности в каждом городе' } },
                                ] },
                                { elem: 'field-block', content: [
                                    { elem: 'field-title', content: 'Текст в подвале баннера(принудительный перенос выделяется так: "\\n")' },
                                    { elem: 'field-input', tag: 'input', attrs: { type: 'text', name: 'footer', value: "Акция действует в Москве и Санкт-Петербурге с 11.11.2015 по 10.01.2016. \nПодробнее на taxi.yandex.ru/vrrules" } },
                                ] },
                                { elem: 'field-block-bgschema', content: [
                                    { elem: 'field-title', content: 'Цветовая схема баннеров' },
                                    { block: 'radiobox',
                                        mods: { size: 's', 'flow': 'vertical' },
                                        name: 'bgschema',
                                        content: [
                                            {
                                                elem: 'radio',
                                                content: 'Жёлтая',
                                                controlAttrs: { value: 'yellow' }
                                            },
                                            '<br>',
                                            {
                                                elem: 'radio',
                                                content: 'Светлая',
                                                controlAttrs: { value: 'white' }
                                            },
                                            '<br>',
                                            {
                                                elem: 'radio',
                                                content: 'Чёрная',
                                                controlAttrs: { value: 'black' }
                                            },
                                            '<br>',
                                            {
                                                elem: 'radio',
                                                content: 'Фото светлая',
                                                controlAttrs: { value: 'photo_light' }
                                            },
                                            '<br>',
                                            {
                                                elem: 'radio',
                                                content: 'Фото тёмная',
                                                controlAttrs: { value: 'photo_dark' }
                                            },
                                        ]
                                    }
                                ] },
                                // { elem: 'field-block-btntype', content: [
                                //     { elem: 'field-title', content: 'Тип кнопки' },
                                //     { block: 'radiobox',
                                //         mods: { size: 's', 'flow': 'vertical' },
                                //         name: 'btntype',
                                //         content: [
                                //             {
                                //                 elem: 'radio',
                                //                 content: 'Жёлтая Скачать',
                                //                 controlAttrs: { value: 'ydownload' }
                                //             },
                                //             '<br>',
                                //             {
                                //                 elem: 'radio',
                                //                 content: 'Жёлтая Скачать приложение',
                                //                 controlAttrs: { value: 'ydownloadapp' }
                                //             },
                                //             '<br>',
                                //             {
                                //                 elem: 'radio',
                                //                 content: 'Жёлтая Установить',
                                //                 controlAttrs: { value: 'yinstall' }
                                //             },
                                //             '<br>',
                                //             {
                                //                 elem: 'radio',
                                //                 content: 'Белая App Store (в макетах кнопки нет, использую чёрную)',
                                //                 controlAttrs: { value: 'was' }
                                //             },
                                //             '<br>',
                                //             {
                                //                 elem: 'radio',
                                //                 content: 'Белая Google Play (в макетах кнопки нет, использую чёрную)',
                                //                 controlAttrs: { value: 'wgp' }
                                //             },
                                //             '<br>',
                                //             {
                                //                 elem: 'radio',
                                //                 content: 'Чёрная App Store',
                                //                 controlAttrs: { value: 'bas' }
                                //             },
                                //             '<br>',
                                //             {
                                //                 elem: 'radio',
                                //                 content: 'Чёрная Google Play',
                                //                 controlAttrs: { value: 'bgp' }
                                //             },
                                //             '<br>',
                                //             {
                                //                 elem: 'radio',
                                //                 content: 'Белая Узнать больше',
                                //                 controlAttrs: { value: 'yknowmore' }
                                //             },
                                //             '<br>',
                                //         ]
                                //     }
                                // ] },
                                { elem: 'field-block', content: [
                                    { elem: 'field-title', tag: 'label', content: 'Фон' },
                                    { elem: 'bg-input-file', tag: 'input', attrs: { 'type': 'file', 'name': 'custombg', 'multiple': '' } }
                                ] },
                                { elem: 'field-block', content: [
                                    { elem: 'field-title', tag: 'label', content: 'Изображение на баннере' },
                                    { elem: 'img-input-file', tag: 'input', attrs: { 'type': 'file', 'name': 'customimg', 'multiple': '' } }
                                ] },
                            ] },
                            { elem: 'button', tag: 'button', content: 'Отправить' },
                        ] },
                    ] }
                ] },
                { block: 'b-banners-label', content: [] },
                { block: 'b-banners', content: [] },
            ]
        }
    ]);
};
