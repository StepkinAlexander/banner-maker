module.exports = function (blocks) {
    blocks.declare('b-error', function (data, page) {
		var block = {
				block: 'b-error',
				content: [
					{ elem: 'label', content: [ data.format, ' ', data.error] },
				]
			}
		return block;
    })
}