/*global BEM */
/*jslint browser:true, jquery:true */
var getCookie = function(name) {
        var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

var availableScales = [
        { scale: '300_250', slide: 0 },
        { scale: '320_480', slide: 0 },
        { scale: '320_50_admob', slide: 0 },
        { scale: '320_50_admob', slide: 1 },
        // { scale: '360_50', slide: 0 },
        // { scale: '360_50', slide: 1 },
        // { scale: '320_533', slide: 0 },
        // { scale: '468_60', slide: 0 },
        // { scale: '468_60', slide: 1 },
        // { scale: '970_90', slide: 0 },
        // { scale: '970_90', slide: 1 },
        // { scale: '336_280', slide: 0 },
        // { scale: '480_32', slide: 0 },
        // { scale: '480_320', slide: 0 },
        // { scale: '600_336', slide: 0 },
        // { scale: '608_320', slide: 0 },
        // { scale: '608_320', slide: 1 },
        // { scale: '320_50', slide: 0 },
        // { scale: '320_50', slide: 1 },
        // { scale: '640_100', slide: 0 },
        // { scale: '640_100', slide: 1 },
        // { scale: '640_134', slide: 0 },
        // { scale: '640_134', slide: 1 },
        // { scale: '640_134', slide: 2 },
        // { scale: '320_100', slide: 0 },
        // { scale: '320_100', slide: 1 },
        // { scale: '640_200', slide: 0 },
        // { scale: '640_200', slide: 1 },
        // { scale: '1242_360', slide: 0 },
        // { scale: '640_960', slide: 0 },
        // { scale: '640_960', slide: 1 },
        // { scale: '728_90', slide: 0 },
        // { scale: '728_90', slide: 1 },
        // { scale: '728_90', slide: 2 },
        // { scale: '800_320', slide: 0 },
        // { scale: '960_576', slide: 0 },
        // { scale: '960_640', slide: 0 },
        // { scale: '960_640', slide: 1 },
        // { scale: '1080_607', slide: 0 },
        // { scale: '1080_1920', slide: 0 },
        // { scale: '1155_400', slide: 0 },
        // { scale: '1155_400', slide: 1 },
        // { scale: '1200_627', slide: 0 },
        // { scale: '1200_1200', slide: 0 },
        // { scale: '1536_2048', slide: 0 },
        // { scale: '768_1024', slide: 0 },
        // { scale: '1920_1080', slide: 0 },
        // { scale: '1024_768', slide: 0 },
        // { scale: '2048_1536', slide: 0 },
    ];

function getBanner(formContent, replyContent, i){
    if(i>=availableScales.length)
        return;

    $.ajax({
        url: '/generate?scale=' + availableScales[i].scale + '&banner=' + availableScales[i].slide,
        type: 'POST',
        data: (formContent) ? formContent : replyContent,
        dataType: 'text',
        contentType: false,
        processData: false,
        success: function (html) {
            var count = $('.b-banner').length;
            var label = 'Идёт генерация баннеров, пожалуйста, подождите...(' + count + '/' + availableScales.length + ')'
            $('.b-banners-label').html(label);

            var content = $('.b-banners').html();
            $('.b-banners').html(content + html);
            if($('.b-banner').length==availableScales.length) {
                $('.b-banners-label').html('Баннеры сгенерированы. Вы можете скачать их себе по <a href="/download">этой ссылке</a>');
                $('.b-form__button').prop('disabled',false);
            }

            i++;
            getBanner(false, replyContent, i);
        },
        error: function (error) {
            var label = 'В процессе генерации произошла ошибка'
            $('.b-banners-label').html(label);
            $('.b-form__button').prop('disabled',false);
        }
    });

    if(!!formContent) {
        formContent = false;
    }
}

$(document).ready(function () {
    $('[name=bgschema][value=yellow]').click()
    $('[name=btntype][value=ydownload]').click()

    $('.b-form').on('submit', function( event ) {
        event.preventDefault();
        $('.b-banners-label').html('Идёт генерация баннеров, пожалуйста, подождите...');
        $('.b-banners').html('');
        $('.b-form__button').prop('disabled',true);


        var formContent =  new FormData(this);

        var csrfToken = getCookie('csrf_token');
        formContent.append('csrf_token',csrfToken)
        formContent.append('new_data',true)

        var replyContent = new FormData();
        replyContent.append('csrf_token',csrfToken)
        getBanner(formContent, replyContent, 0);
    });
});