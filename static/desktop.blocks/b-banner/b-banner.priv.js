module.exports = function (blocks) {
    blocks.declare('b-banner', function (data, page) {
		var block = {
				block: 'b-banner',
				content: [
					{ elem: 'label', content: [ data.format, ' ', data.comment ] },
					{ elem: 'banner', tag: 'img', attrs: { src: data.image, style: (data.border!='') ? 'border:solid 1px;border-color:{0};'.format(data.border) : '' } }, 
				]
			}
		return block;
    })
}