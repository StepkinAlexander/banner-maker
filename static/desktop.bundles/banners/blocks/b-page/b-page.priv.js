module.exports = function (blocks) {
    blocks.declare('b-page', function (data, page) {
    	var block = [];
    	for(var i=0; i<data.length; i++)
        	block.push(blocks.exec('b-banner', data[i]));

        return block;
    });
}
