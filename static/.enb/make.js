'use strict';

const autoprefixer = {
    // Код в *.css должен работать на
    //  - всех поддерживаемых touch-устройствах
    //  - всех поддерживаемых десктопных браузерах, кроме поддерживаемых версий IE (для них отдельные правила)
    'desktop:css': {
        browsers: [
            'last 2 Chrome version',
            'last 2 Firefox version',
            'last 2 Safari version',
            'Opera 12.1',
            'last 1 Opera version',
            'ie >= 10',

            'android >= 4',
            'ios >= 5.1'
        ]
    },

    // Код в *.ieN.css должен работать в соответствующей версии IE
    'desktop:ie8.css': {browsers: 'ie 8'},
    'desktop:ie9.css': {browsers: 'ie 9'},

    // Код в *.css должен работать на всех поддерживаемых touch-браузерах, кроме IE 9 (для него отдельное правило)
    'touch-pad:css': {browsers: 'android >= 2.3, ios >= 5.1, ie >= 10'},
    'touch-phone:css': {browsers: 'android >= 2.3, ios >= 5.1, ie >= 10'},

    // Код в *.ie9.css должен работать в IE 9
    'touch-pad:ie9.css': {browsers: 'ie 9'},
    'touch-phone:ie9.css': {browsers: 'ie 9'}
};

const islands = require('islands');
const levels = require('enb-bem-techs/techs/levels');
const files = require('enb-bem-techs/techs/files');
const copy = require('enb/techs/file-copy');
const provider = require('enb/techs/file-provider');
const merge = require('enb/techs/file-merge');
const deps = require('enb-bem-techs/techs/deps');
const depsByTechToBemdecl = require('enb-bem-techs/techs/deps-by-tech-to-bemdecl');
const js = require('enb-js/techs/browser-js');
const stylus = require('enb-stylus/techs/stylus');
const borschik = require('enb-borschik/techs/borschik');
const i18nKeysets = require('enb-bem-i18n/techs/keysets');
const i18nLang = require('enb-bem-i18n/techs/i18n');
const bhBundle = require('enb-bh/techs/bh-bundle');
const privServer = require('enb-priv-js/techs/priv-server');
const privServerInclude = require('enb-priv-js/techs/priv-server-include');
const smartCopy = require('enb-smart-copy/techs/smart-copy');

function use(tech, params) {
    return [
        tech,
        params || {}
    ];
}

module.exports = function (config) {
    // Языки для сборки ['ru', 'en', 'uk', 'be', 'kk', 'tt', 'tr']
    config.setLanguages(['ru']);

    // все бандлы расположены в blocks
    config.nodes('*.bundles/*');

    // конфиг для development окружения
    config.mode('development', () => {
        config.nodeMask(/.+\.bundles\/.*/, nodeConfig => {
            nodeConfig.addTechs([
                // привы собираем без склеивания в один файл
                use(privServer, {target: '?.priv.js'}),

                // копируем языковой js
                use(copy, {sourceTarget: '?.{lang}.js', destTarget: '_?.{lang}.js'}),

                // копруем единый склееный css-файл в конечную папку (то же самое со стилями для IE)
                use(copy, {sourceTarget: '?.css', destTarget: '_?.css'}),
                use(copy, {sourceTarget: '?.ie8.css', destTarget: '_?.ie8.css'}),
                use(copy, {sourceTarget: '?.ie9.css', destTarget: '_?.ie9.css'})
            ]);
        });
    });

    // конфиг для production окружения
    config.mode('production', () => {
        config.nodeMask(/.+\.bundles\/.*/, nodeConfig => {
            nodeConfig.addTechs([
                // привы склеиванием в один файл
                use(privServerInclude, {target: '?.priv.js'}),

                // прогоняем языковой js через борщик
                use(borschik, {sourceTarget: '?.{lang}.js', destTarget: '_?.{lang}.js', minify: true, freeze: true}),

                // прогоняем единый css через борщик (то же самое со стилями для IE)
                use(borschik, {
                    sourceTarget: '?.css',
                    destTarget: '_?.css',
                    minify: true,
                    freeze: true,
                    tech: 'cleancss'
                }),
                use(borschik, {
                    sourceTarget: '?.ie8.css',
                    destTarget: '_?.ie8.css',
                    minify: true,
                    freeze: true,
                    tech: 'cleancss'
                }),
                use(borschik, {
                    sourceTarget: '?.ie9.css',
                    destTarget: '_?.ie9.css',
                    minify: true,
                    freeze: true,
                    tech: 'cleancss',
                    techOptions: {cleancss: {advanced: false}}
                })
            ]);

            // копируем все необходимые собраные файлы в конечную папку
            nodeConfig.addTargets([
                'public',
                'i18n-static.{lang}',
                'i18n-public.{lang}'
            ]);

            nodeConfig.addTechs([
                use(smartCopy, {
                    sourceTargets: ['_?.css', '_?.ie8.css', '_?.ie9.css'],
                    dest: '../out/public',
                    target: 'public'
                }),
                use(smartCopy, {
                    sourceTargets: ['_?.{lang}.js'],
                    dest: '../out/public',
                    target: 'i18n-public.{lang}',
                    lang: '{lang}'
                }),
                use(smartCopy, {
                    sourceTargets: ['?.{lang}.priv.js', '?.{lang}.bh.js'],
                    dest: '../out/static',
                    target: 'i18n-static.{lang}',
                    lang: '{lang}'
                })
            ]);
        });
    });

    // Список бандлов, которые будем собирать
    [
        'desktop'
        // 'mobile'
    ].map(bundleType => {
        // Конфигурируем каждый бандл
        config.nodes(`${bundleType}.bundles/*`, nodeConfig => {
            // Формируем список файлов, которые будут учавствовать в сборке. Это необходимо, чтобы в дальнейшем совершать с ними какие-либо действия.
            nodeConfig.addTechs([
                // Используем технологии, чтобы получить список файлов, которые будут участвовать в сборке.
                // (1) Пробегает по уровням бандла, пишет их в ?.levels
                use(levels, {target: '?.levels', levels: getPlatformLevels(bundleType)}),
                // (2) Пробегает по блокам и пишет их в ?.bemdecl.js
                use(provider, {target: '?.bemdecl.js'}),
                // (3) Собирает зависимости блоков из ?.bemdecl.js с учетом уровней из ?.levels и пишет их в ?.deps.js
                use(deps, {target: '?.deps.js', levelsTarget: '?.levels', bemdeclFile: '?.bemdecl.js'}),
                // (4) Формирует список файлов (?.files) и директорий (?.dirs) на основе ?.levels и ?.deps.js
                use(files, {
                    filesTarget: '?.files',
                    dirsTarget: '?.dirs',
                    levelsTarget: '?.levels',
                    depsFile: '?.deps.js'
                }),

                // Браузерные JS
                // (1) Склеивает файлы .js, .vanilla.js и .browser.js в единый ?.browser.js
                use(js, {target: '?.browser.js'}),

                // Файлы переводов
                // (1) В директориях *.i18n ищет файлы *.i18n.js и склеивает их в ?.keysets.{lang}.js
                use(i18nKeysets, {target: '?.keysets.{lang}.js', lang: '{lang}'}),
                // (2) Собирает файл ?.lang.{lang}.js из файла ?.keysets.{lang}.js
                use(i18nLang, {
                    target: '?.lang.{lang}.js',
                    keysetsFile: '?.keysets.{lang}.js',
                    lang: '{lang}',
                    exports: {
                        globals: 'force', // нужно форсить глобальный экспорт i18n для BH шаблонов
                        commonJS: false,  // без этой опции технология i18n ломает export BH из .{lang}.bh.js
                        ym: false         // модульная система (пока) не используется
                    }
                }),

                // Cерверные BH
                use(bhBundle, { // (1) Собирает шаблоны из файлов *.bh.js, найденых в списке файлов ?.files в файл ?.bh.js
                    target: '?.bh.js',
                    mode: 'production',
                    bhOptions: {
                        jsAttrName: 'data-bem',
                        jsAttrScheme: 'json',
                        jsElem: false
                    },
                    requires: {
                        i18n: {
                            globals: 'BEM.I18N'
                        }
                    }
                }),
                use(merge, { // (2) Склеивает файлы ?.bh.js и ?.lang.{lang}.js в файл ?.{lang}.bh.js
                    target: '?.{lang}.bh.js',
                    sources: ['?.lang.{lang}.js', '?.bh.js'],
                    lang: '{lang}'
                }),

                // Собираем CSS
                use(stylus, { // обрабатывает .styl файлы и склеивает все стили в один файл
                    target: '?.css',
                    autoprefixer: autoprefixer['desktop:css']
                }),
                use(stylus, { // то же но для IE8
                    target: '?.ie8.css',
                    sourceSuffixes: ['styl', 'css', 'ie.styl', 'ie.css', 'ie8.styl', 'ie8.css'],
                    autoprefixer: autoprefixer['desktop:ie8.css']
                }),
                use(stylus, { // то же но для IE9
                    target: '?.ie9.css',
                    sourceSuffixes: ['styl', 'css', 'ie9.styl', 'ie9.css'],
                    autoprefixer: autoprefixer['desktop:ie9.css']
                }),

                // Клиентские BH
                // (1) Собирает файл ?.client.bemdecl.js на основе файла ?.deps.js
                use(depsByTechToBemdecl, {
                    sourceSuffixes: 'deps.js',
                    target: '?.client.bemdecl.js',
                    sourceTech: 'js',
                    destTech: 'bh-bundle'
                }),
                // (2) Собирает клинетские депсы в файл ?.client.deps.js на основе файла ?.client.bemdecl.js
                use(deps, {bemdeclFile: '?.client.bemdecl.js', target: '?.client.deps.js', levelsTarget: '?.levels'}),
                // (3) Собирает список клиентских файлов в ?.client.files и клиентские директории в ?.client.dirs
                use(files, {filesTarget: '?.client.files', dirsTarget: '?.client.dirs', depsFile: '?.client.deps.js'}),
                // (4) Собирает клиентские шаблоны из файлов *.bh.js, найденых в списке файлов ?.client.files в файл ?.client.bh.js
                use(bhBundle, {
                    filesTarget: '?.client.files',
                    target: '?.client.bh.js',
                    mode: 'production',
                    mimic: ['BEMHTML'],
                    bhOptions: {
                        jsAttrName: 'data-bem',
                        jsAttrScheme: 'json',
                        jsElem: false
                    },
                    requires: {
                        i18n: {
                            globals: 'BEM.I18N'
                        }
                    }
                }),
                // (5) Склеивает все клиентские JS файлы (?.browser.es5.js, ?.client.bh.js, ?.lang.{lang}.js) в единый ?.{lang}.js
                use(merge, {
                    lang: '{lang}',
                    sources: ['?.browser.js', '?.lang.{lang}.js', '?.client.bh.js'],
                    target: '?.{lang}.js'
                }),

                // Priv
                // (1) Берет склеенные привы из файла ?.priv.js и склеивает их с файлом переводов ?.{lang}.lang.js. Результат пишет в файл ?.{lang}.priv.js
                use(merge, {
                    lang: '{lang}',
                    sources: ['?.lang.{lang}.js', '?.priv.js'],
                    target: '?.{lang}.priv.js'
                })
            ]);

            // На выходе мы хотим получить следующие файлы
            nodeConfig.addTargets([
                '_?.css',
                '_?.ie8.css',
                '_?.ie9.css',
                '_?.{lang}.js',
                '?.{lang}.priv.js',
                '?.{lang}.bh.js'
            ]);
        });
    });

    function getPlatformLevels(platform) {
        const platformLevels = {
            'desktop': [
                {path: 'node_modules/islands-promo/common.blocks', check: false},
                {path: 'node_modules/islands-promo/desktop.blocks', check: false},

                {path: 'common.blocks', check: true},
                {path: `${platform}.blocks`, check: true}
            ],
            'touch-phone': [],
            'touch-pad': []
        };

        const projectLevels = platformLevels[platform].map(l => config.resolvePath(l));

        return islands.getPlatformLevels(platform)
            .concat(projectLevels);
    }
};
