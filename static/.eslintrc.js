module.exports = {
    env: {
        browser: true
    },
    parserOptions: {
        ecmaFeatures: {
            globalReturn: false
        }
    }
};
