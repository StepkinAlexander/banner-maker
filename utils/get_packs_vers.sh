#!/bin/bash
# скрипт шерстит две папки с npm модулями и выдаёт версии(по грепу строки с version)
# вхождений из первого каталога с аналогичными во втором
for d in $1/*
do
	PACKAGE=${d:${#1}+1}
	echo "Пакет $PACKAGE:>>>>>>>>>>>>>."
	FILE1="$1/$PACKAGE/package.json"
	FILE2="$2/$PACKAGE/package.json"
	echo "---пакет из $1:"
	cat $FILE1 | grep version
	echo "---пакет из $2:"
	cat $FILE2 | grep version
done